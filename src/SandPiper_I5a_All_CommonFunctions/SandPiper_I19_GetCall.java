package SandPiper_I5a_All_CommonFunctions;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;




import Utilities_i5A_All.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SandPiper_I19_GetCall {
	
	public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	       	
	        	Thread.sleep(5000);
	        	
	        	
	        	for (int i=0;i<10;i++){
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        
	        
	        int ItemCount=jarray.size();
	        
	        int resCount=0;
	        
	        for(int k=0; k<jarray.size(); k++) {
	        	
	        	jsonobject = jarray.get(k).getAsJsonObject();
		       
	        	
	        	
	        	String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 7) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	//break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
					
					resCount=resCount+1;
					
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	//break;
		        }
	        	
	        }
	        	  
	        
	        if(ItemCount==resCount) {
	        	
	        	res=true;
	        	break;
	        }
	        
	      }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
		}
			
		
	    
	    finally{
	        
	        response.close();
	        return res;
	        
	}
	    
	 
	    
		
	}
	
public static boolean GetCallItemStatusValidationOnlyConfirmed(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(waitingTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne != 7) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is not 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	break;
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}

}
