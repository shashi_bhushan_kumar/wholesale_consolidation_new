package SandPiper_I5a_All_CommonFunctions;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import Utilities_i5A_All.utilityFileWriteOP;

public class ShipCall {
	
	/*public static boolean ShipCallOrderShip(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid) throws IOException {
		
		boolean res = false;
		
		String ItemID[] = AllItemIDs.split(",");
		String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
		
		CloseableHttpResponse response=null;
        
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
        credsProvider.setCredentials(
        		new AuthScope(ProxyHostName, ProxyPort),
        		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

        try {
			
        	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
            HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
            
            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();
            
            HttpPost httppostship = new HttpPost(UrlTail + "/" + OrderID + "/" + "@ship?" + ApiKey);
            
            httppostship.setConfig(config);
            
            httppostship.addHeader(AuthorizationKey, AuthorizationValue);
            
            System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
            
            String json = "{\"id\": \"ASN00001\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [{\"itemId\": \"" + ItemID[0] + "\",\"quantityShipped\": " + QuantityOrdered[0] + "},{\"itemId\": \"" + ItemID[1] + "\",\"quantityShipped\": " + QuantityOrdered[1] + "}]}";
            		  
            httppostship.setEntity(new StringEntity(json));
            
            httppostship.setHeader("Accept", "application/json");
			httppostship.setHeader("Content-type", "application/json");
			
			utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
			response = httpclient.execute(target, httppostship);
	        

            System.out.println("Response code is "+response.getStatusLine().getStatusCode());
            utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode());

            System.out.println(EntityUtils.toString(response.getEntity()));
            utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()));
            
            if(response.getStatusLine().getStatusCode() == 202)
				res = true;
			else
				res=false;
            
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
        finally{
        	response.close();
        }
        
        return res;
	}*/
	
public static boolean ShipCallOrderShip(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid) throws IOException {
		
		boolean res = false;
		
		String ItemID[] = AllItemIDs.split(",");
		String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
		
		String ItemDetails = "";
		
		CloseableHttpResponse response=null;
        
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
        credsProvider.setCredentials(
        		new AuthScope(ProxyHostName, ProxyPort),
        		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

        try {
			
        	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
            HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
            
            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();
            
            HttpPost httppostship = new HttpPost(UrlTail + "/" + OrderID + "/" + "@ship?" + ApiKey);
            
            httppostship.setConfig(config);
            
            httppostship.addHeader(AuthorizationKey, AuthorizationValue);
            
            /*
            
{
  "id": "sri2-10",
  "params": {
    "label": {
      "print": true,
      "copies": 1,
      "device": "prnt-138493"
    }
  },
  "items": [
    {
      "itemId": "101703531",
      "quantityShipped": 2
    }
   
  ]
}    
            

            
            */
            
            System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
            
            for(int i = 1; i<= ItemID.length; i++) {
            	
            	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"quantityShipped\": " + QuantityOrdered[i-1] + "}";
   
            	if(i!=ItemID.length)
            		ItemDetails = ItemDetails + ",";
            }
            
            String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
            System.out.println(json);		  
            httppostship.setEntity(new StringEntity(json));
            
            httppostship.setHeader("Accept", "application/json");
			httppostship.setHeader("Content-type", "application/json");
			
			utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
			response = httpclient.execute(target, httppostship);
	        

            System.out.println("Response code is "+response.getStatusLine().getStatusCode());
            utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode());

            String responsebody = EntityUtils.toString(response.getEntity());
            System.out.println(responsebody);
            
            utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody);
            
            if(response.getStatusLine().getStatusCode() == 202)
				res = true;
			else
				res=false;
            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
        finally{
        	response.close();
        }
        
        return res;
	}

}
