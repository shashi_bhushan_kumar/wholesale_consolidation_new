package SandPiper_I5a_All_CommonFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities_i5A_All.TakeScreenShot;
import Utilities_i5A_All.getCurrentDate;
import Utilities_i5A_All.utilityFileWriteOP;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SandPiper_I5a_Utilities {
	
public static ArrayList<HashMap<String, String>> TemporaryOrderIDExtract(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String NasPath, String LocalPath, String OrderID, String tcid) throws IOException {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;
		
		//ArrayList<ArrayList<Map.Entry<String, String>>> ListofMaps = new ArrayList<ArrayList<Map.Entry<String,String>>>();
		
		ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> hm = null;
		
		ArrayList<String> HeaderList = null;
		
		//String Result = null;
		//int flag = 0;
		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
		/*	sftpchannel.cd("cd..;");
			sftpchannel.cd("cd..;");*/
			
			
			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
				//Date currentfilelastmodifieddate = new Date(new File(filename).lastModified());
				//System.out.println(entry.getAttrs().getMTime());
				long currentfilelastmodifiedmilliseconds = (long) entry.getAttrs().getMTime();
				
				if(currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {
					
					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;

				}
					
				
			}
			System.out.println(lastmodifiedfilename +"  "+ LocalPath + lastmodifiedfilename);
			sftpchannel.get(lastmodifiedfilename, LocalPath + lastmodifiedfilename);//Copy file from WinSCP to local

				//Read from local
				br = new BufferedReader(new FileReader(LocalPath + lastmodifiedfilename));
				System.out.println(lastmodifiedfilename +" "+ lastmodifiedfiletime);
				
				String CurrentLine = null;
				int linecount = 0;
				
				while((CurrentLine = br.readLine()) != null) {
					
					//ArrayList<Map.Entry<String, String>> FileKeyValues = new ArrayList<Map.Entry<String,String>>();
					hm = new HashMap<String, String>();
					//Split the Current line in comma seperated values
					String[] strarr = CurrentLine.split(",");
					
					if(linecount==0) {
						
						HeaderList = new ArrayList<String>();
						
						for(int i=0; i<strarr.length; i++){
							HeaderList.add(strarr[i]);
						}
						
						
					}
				
					if(linecount > 0) {
						
						
						
						for(int j=0; j<strarr.length; j++) {
							
							//FileKeyValues.add(new AbstractMap.SimpleEntry(HeaderList.get(j), strarr[j]));
							//System.out.println("About to put "+HeaderList.get(j)+ " and" +strarr[j]);
							hm.put(HeaderList.get(j), strarr[j]);
						}
						
						ListofMaps.add(hm);
						System.out.println("added hashmap to arraylist "+hm.size() + hm.get("messageCreatedAt"));

					}
					
					

					linecount++;
					
					//TempOrderIDList.add(strarr[0]);
					

					
				}
				
			
			br.close();
			FileUtils.cleanDirectory(new File(LocalPath)); // Delete all downloaded files from local directory
			
			
			sftpchannel.exit();
			
			session.disconnect();
			
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);
			return ListofMaps;
		}
		
		
		
	
		
		return ListofMaps;
	}

public static long LastModifiedFileTime(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String NasPath, String TemporaryFilePath, String tcid) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	//BufferedReader br = null;

	long lastmodifiedfiletime = 0;
	//String Result = null;
	//int flag = 0;
	
	try {
		
		session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(SFTPPassword);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
				
		sftpchannel.cd(NasPath);
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			
			
			
			sftpchannel.get(filename, TemporaryFilePath + filename);//Copy file from WinSCP to local
			
			Date currentfilelastmodifieddate = new Date(new File(TemporaryFilePath + filename).lastModified());

			long currentfilelastmodifiedmilliseconds = currentfilelastmodifieddate.getTime();

			if(currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {

				lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;

			}
			
		
			
		}
		
		FileUtils.cleanDirectory(new File(TemporaryFilePath)); // Delete all downloaded files from local directory
		
		//br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);
		return lastmodifiedfiletime;
	}
	
	
	

	
	return lastmodifiedfiletime;
}


	public static ArrayList<String> OrderIDListExtract(String DriverSheetPath, String SheetName) {
		
		ArrayList<String> OrderIDList = new ArrayList<String>();
		
		String OrderID = null;
		
		int r = 0;
		
		try {
			
			int rows = 0;
			
			Workbook wrk1 = Workbook.getWorkbook(new File(DriverSheetPath));
			
			Sheet sheet1 = wrk1.getSheet(SheetName);
			
			rows = sheet1.getRows();
			
			for(r=1; r<rows; r++) {
				
				OrderID = sheet1.getCell(16, r).getContents().trim();
				
				OrderIDList.add(OrderID);
			}
			
			
			return OrderIDList;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			OrderIDList = null;
			return OrderIDList;
		}
		
		
	}
	
	public static boolean sandpipervalidateordersInRDS(String orderid,String[] columnname,String[] itemid,ArrayList list, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=false;
		 boolean res1=true;;
		 
		// String[] itemid
		 
		 ArrayList<Map> itemlist=list;
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        for(int item=0;item<itemlist.size();item++)
	        {
	        for(int col=0;col<columnname.length;col++)
	        {
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	        if(columnname[col].equals("orderId"))
	        {
	        	columnname[col]="temporaryorderId";
	        }
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        
	        
	     //   Utilities.utilityFileWriteOP.writeToLog(tcid, "Order Details in RDS --"+QResult, "");
	    	 

	        
	        String[] rest= QResult.split(columnname[col]);
			 String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 //System.out.println("QResult is "+QResult+" is this!!!");
			 //System.out.println("justfinalres "+justfinalres);
	    	 String[] rest2=justfinalres.split("[\\r\\n]+");
	    	 /*
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 }*/
	    	 /*String finalres="";
	    	 if(col==0 && rest2.length>5)
	    	 {
	    		 finalres=rest2[5].trim();
	    	 }
	    	 else
	    	 {
	    	 finalres=rest2[2].trim();
	    	 }*/
	    	 String finalres="";
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 if(rest2[restcount].contains("("))
	    	 {
	    		 if(!rest2[restcount-1].equals(columnname))
	    		 {
	    		 finalres=rest2[restcount-1].trim();
	    		 
	    		 break;
	    		 }
	    	 }
	    	 
	    	 }
	    	 System.out.println("finalres "+finalres);
	    	 
	    	 //System.out.println(columnname[col]);
	    	 
	    	 //System.out.println(itemid[item]);
	    	//System.out.println(itemlist.get(item).toString());
	    	//System.out.println(columnname[col]);
	    	 String nasres="";
	    	 
	    	 if(columnname[col].equalsIgnoreCase("temporaryorderid"))
	    	 {
	    		 nasres=itemlist.get(item).get("orderId").toString().replaceAll("[-+|]", "");
		    	 
	    	 }
	    	 else
	    	 {
	    		 nasres=itemlist.get(item).get(columnname[col]).toString().replaceAll("[-+|]", "");
		    	  
	    	 }
	    	 
	    	 if(nasres.equalsIgnoreCase(finalres))
	    	 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Matched with the value found in RDS DB "+finalres);
	    	
	    	 
	    	     res=true;
	    	 
	    	 
	    	 
	    	 }
	    	 else
	    	 {		
	    		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	    		 {
	    			 
	    			 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match because NO value was found in RDS DB ");
		    		 
	    		 }
	    		 else
	    		 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match with the value found in RDS DB "+finalres);
	    		 }
	    		 
	    		 
	    		 res=false;
	    	 }
	    	 
	    	 
	    	 
	    	 
	    	 res1=res1&&res;
	    	 
	    	 
	        }
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

 }
	              

	        
catch(Exception e){
e.printStackTrace();
 System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		    
		    
		    System.out.println("NAS validation res is "+res1);
		
	      return res1;
	}

	}
	       
	
	
	
	public static boolean ValidateTemporaryOrderIDWithOrderIDInRDS(String orderid,String temp_orderid, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=false;
		
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	          

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	      
//select distinct orderid from orderconsolidations where temporaryorderId ='TB-37620180216';  
	        
	        
	        
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct orderid from orderconsolidations where temporaryorderId ='"+temp_orderid+"' ;\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        
	      if(QResult.contains(orderid)){
	    	  
	    	  res=true;
	    	  
	    	  
	      }
	        
	        
	        
	     System.out.println("Orders mapped with Temporary Order ID   "+temp_orderid+" in RDS is  "+QResult); 

	     Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Orders mapped with Temporary Order ID in RDS is -- --"+QResult, "");
	    	 

	  
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);


	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }

		
	      return res;
	}

}
	       

	
	public static boolean sandpipervalidateordersInRDSFromExcel(String orderid,String[] columnname,String[] itemid,ArrayList list, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=false;
		 boolean res1=true;;
		 
		// String[] itemid
		 
		 ArrayList<Map> itemlist=list;
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        for(int item=0;item<itemlist.size();item++)
	        {
	        	

	
	        for(int col=0;col<columnname.length;col++)
	       
	        
	        {
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	        if(columnname[col].equals("orderId"))
	        {
	        	columnname[col]="temporaryorderId";
	        }
	        
	        
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	              }
	          
	        QResult=new String(tmp, 0, i); 
	        
	        
	     //   Utilities.utilityFileWriteOP.writeToLog(tcid, "Order Details in RDS --"+QResult, "");
	    	 

	        
	        String[] rest= QResult.split(columnname[col]);
			 String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 //System.out.println("QResult is "+QResult+" is this!!!");
			 //System.out.println("justfinalres "+justfinalres);
	    	 String[] rest2=justfinalres.split("[\\r\\n]+");
	    	 /*
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 }*/
	    	 /*String finalres="";
	    	 if(col==0 && rest2.length>5)
	    	 {
	    		 finalres=rest2[5].trim();
	    	 }
	    	 else
	    	 {
	    	 finalres=rest2[2].trim();
	    	 }*/
	    	 String finalres="";
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 if(rest2[restcount].contains("("))
	    	 {
	    		 if(!rest2[restcount-1].equals(columnname))
	    		 {
	    		 finalres=rest2[restcount-1].trim();
	    		 
	    		 break;
	    		 }
	    	 }
	    	 
	    	 }
	    	 System.out.println("finalres "+finalres);
	    	 
	    	 //System.out.println(columnname[col]);
	    	 
	    	 //System.out.println(itemid[item]);
	    	//System.out.println(itemlist.get(item).toString());
	    	//System.out.println(columnname[col]);
	    	 String nasres="";
	    	 
	    	 if(columnname[col].equalsIgnoreCase("temporaryorderid"))
	    	 {
	    		 nasres=itemlist.get(item).get("orderId").toString().replaceAll("[-+|]", "");
		    	 
	    	 }
	    	 else
	    	 {
	    		 nasres=itemlist.get(item).get(columnname[col]).toString().replaceAll("[-+|]", "");
		    	  
	    	 }
	    	 
	    	 if(nasres.equalsIgnoreCase(finalres))
	    	 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Matched with the value found in RDS DB "+finalres);
	    	
	    	 
	    	     res=true;
	    	 
	    	 
	    	 
	    	 }
	    	 else
	    	 {		
	    		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	    		 {
	    			 
	    			 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match because NO value was found in RDS DB ");
		    		 
	    		 }
	    		 else
	    		 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match with the value found in RDS DB "+finalres);
	    		 }
	    		 
	    		 
	    		 res=false;
	    	 }
	    	 
	    	 
	    	 
	    	 
	    	 res1=res1&&res;
	    	 
	    	 
	        }
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		    
		    
		    System.out.println("NAS validation res is "+res1);
		
	      return res1;
	}

	}
	       

	
	
	public static boolean sandpiperOrderDetailsInRDS_Screenshot(String orderid,String[] columnname, String[] itemid, ArrayList list, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 ArrayList<Map> itemlist=list;
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 

	        //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	         String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select * from orderconsolidations where orderid ='"+orderid+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 2048);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        
	        
	    Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Order Details in RDS --"+QResult, "");

	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	     

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}

	}
	  
	
	
	
	
	
	
	
	
	
	
	

	public static boolean sandpipervalidateordersInRDS(String orderid,String columnname, String[] itemid, String columnvalue, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        for(int item=0;item<itemid.length;item++)
	        {
	        
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct "+columnname+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        String[] rest= QResult.split(columnname);
			 String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 System.out.println("QResult is "+QResult+" is this!!!");
			 System.out.println("justfinalres "+justfinalres+" is this!");
	    	 String[] rest2=justfinalres.split("[\\r\\n]+");
	    	 String finalres="";
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 if(rest2[restcount].contains("("))
	    	 {
	    		 if(!rest2[restcount-1].equals(columnname))
	    		 {
	    		 finalres=rest2[restcount-1].trim();
	    		 
	    		 break;
	    		 }
	    	 }
	    	 
	    	 }
	    	 System.out.println("finalres "+finalres);
	    	 //System.out.println(columnname[col]);
	    	 
	    	 //System.out.println(itemid[item]);
	    	//System.out.println(itemlist.get(item).toString());
	    	//System.out.println(columnname[col]);
	    	 
	    	 if(finalres.equalsIgnoreCase(columnvalue))
	    	 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" from NAS file "+columnvalue, "Matched with the value found in RDS DB "+finalres);
	    		 
	    		 
	    	 }
	    	 else
	    	 {		
	    		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	    		 {
	    			 
	    			 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+columnvalue, "Did not match because NO value was found in RDS DB ");
		    		 
	    		 }
	    		 else
	    		 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+columnvalue, "Did not match with the value found in RDS DB "+finalres);
	    		 }
	    		 res=false;
	    	 }
	    	 
	    	 
	        
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}

	}
	
	
	public static boolean aws_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw , String Screenshotpath , String Resultpath,XWPFRun xwpfrun)
	{
		boolean res=false;
		
		try
		{
			
			Thread.sleep(10000);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
			Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Login Page of AWS Rackspace", "Displayed!!!",Resultpath);
			
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			
			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
			Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath);
			
			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);
			Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath);
			
			String awsDynamoDB_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			
			driver.findElement(By.xpath("//a[@id='signin_button']")).click();
			
			
			Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Signin  Button", "Clicked!!!",Resultpath);
			
			
			
			
			
			
			if(driver.findElement(By.xpath("//span[text()='AWS Management Console']")).isDisplayed())
			{
				String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
				
				res=true;
				Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "AWS Management Console Page", "Displayed!!!",Resultpath);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while logging in AWS Rackspace", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}
		
	}
	
	
	public static boolean Ecs_task_runner_job_trigger_task_ID_Retrieve(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath , String Screenshotpath,XWPFRun xwpfrun,String[] taskid)
	{
		{
			String[] tasknames= taskname.split(",");
			boolean res=false;
			try{

				// Take Screenshot

				String AfterAWSLogin_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);

				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).isDisplayed();

				if(imgres1)
				{
					utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);
					// Take Screenshot
					
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					
					//Enter ecs keyword in the search window
					driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(searchkey);
					utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath);
					driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(Keys.RETURN);
							
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
					// Take Screenshot
					
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					
					
					
					//loop for the number of jobs to run
					
					for(int i=0;i<tasknames.length;i++){
					
					//click on task definitions				
					driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
					
					//driver.findElement(By.xpath("//a[@id='side-nav-item-taskDefinitions']")).click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));
					driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
					WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
					JavascriptExecutor executor2 = (JavascriptExecutor)driver;
					executor2.executeScript("arguments[0].click();", element2);		
										
					//loop
					
					boolean taskvisible =false;
					
					
					driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);
					utilityFileWriteOP.writeToLog(TC_no, "Task name: "+tasknames[i], "Entered!!!",Resultpath);
					int count=0;
					driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					while(taskvisible!=true){
						try{
						taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();
						}
						catch(Exception e){
						System.out.println("Visibilty of task"+taskvisible);
						driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();
						count++;
						if(count>15)
							break;
						}
					}
					
					
					System.out.println("task found");
					//<i class="awsui-icon angle-right" ng-click="pagination.pageForward()" ng-class="{'awsui-icon-disabled': pagination.pageForwardDisabled()}"></i>
					
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					
					
					driver.findElement(By.xpath("(//div[contains(@class,'ui-grid-selection-row-header-buttons ui-grid-icon-ok ng-scope')])[1]")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
					driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
					driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
					// Take Screenshot
					String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
					utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
					utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
					Thread.sleep(2000);
					// Take Screenshot
					String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
					
					WebElement element = driver.findElement(By.xpath("//a[contains(text(),'nonprod-ECS-ECSCluster-W68V9ZHCS399')]"));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					System.out.println("Selected cluster");
					utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
					Thread.sleep(2000);
					// Take Screenshot
					String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
					
					//driver.findElement(By.xpath("//span[contains(text(),'Run Task')]")).click();
					WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
					//WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Cancel')]"));
					JavascriptExecutor executor1 = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element1);
					//driver.findElement(By.xpath("//span[contains(text(),'Cancel')]")).click();
					//System.out.println("Clicked Cancel");
					System.out.println("Clicked Run Task");
					
					//	driver.findElement(By.xpath("//button[@class='awsui-button awsui-button-size-normal awsui-button-variant-link awsui-hover-child-icons']")).click();
					//  Take Screenshot
					String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(TaskTriggered, driver);
					utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
					//  Take Screenshot
					String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(TaskSuccess, driver);
					
					taskid[0]=driver.findElement(By.xpath("//div/span/div[contains(text(),'Task Ids')]")).getText().replace("Task Ids : [", "").replace("\"", "").replace("]", "");
					System.out.println("Task ID: "+taskid[0]);
					System.out.println("Transfer Request task Triggered Successfully");
					Thread.sleep(1000);
					}
					driver.close();
					Thread.sleep(2000);
					
					utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					res=true; 		
					return res;
					
				}
				
				else
				{
					res =false;
					return res;
				}
				
			}
			catch (Exception e) 
			{	
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
				return res;
			}

			finally 
			{
				return res;
			}


		}
		
	}	
	
	
	public static boolean AWSLogOut (String UserName,WebDriver driver, WebDriverWait wait, String TC_no , String Resultpath ,  String Screenshotpath,XWPFRun xwpfrun) 
	{
		boolean res = false;
		try
		{
			String MainPg_title = "Fanatical Support for AWS";
			String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
			System.out.println("Current Title: "+driver.getTitle());
			for(String winHandle : driver.getWindowHandles())
			{
				if (driver.switchTo().window(winHandle).getTitle().equals(MainPg_title))  //Search for the desired Window
				{

					
					System.out.println("Entered to this block working correctly");
					break;

				} 
				else 
				{
					
					driver.switchTo().window(currentWindow);
				} 
			}


			
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='rs-dropdown-title' and contains(text(),' "+ UserName + "' )]")));
			driver.findElement(By.xpath("//div[@class='phx-dropdown-title' and contains(text(),'"+UserName.toUpperCase()+"')]")).click();

			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")));
			driver.findElement(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")).click();
			Thread.sleep(3000);
			//Take Screenshot
			String ClickonLogout=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";   
			TakeScreenShot.saveScreenShot(ClickonLogout, driver);
			utilityFileWriteOP.writeToLog(TC_no, "Logout Button Clicked", "Done!!!",Resultpath);
			Thread.sleep(5000);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));

			String AfterLogout_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";

			TakeScreenShot.saveScreenShot(AfterLogout_WP1, driver);

			if(driver.findElement(By.xpath("//input[@id='username']")).isDisplayed())
			{
				res=true;
			}


			if(res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Logout", "Successfull!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Pass");
			}
			if(!res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Cannot Logout", "Failed to Logout!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Fail");
			}


			return res;


		}

		catch (Exception e)
		{
			res=false;
			System.out.println("The error is "+e);
			utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for"+e, "while logging out from AWS",Resultpath);
			return res;
		}

		finally
		{
			return res;
		}
	}
	
	
	
	

	
}
