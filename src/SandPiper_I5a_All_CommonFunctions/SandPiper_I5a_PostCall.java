package SandPiper_I5a_All_CommonFunctions;

import java.io.File;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import Utilities_i5A_All.utilityFileWriteOP;

public class SandPiper_I5a_PostCall {
	
	public static boolean PostCallOrderCreationGeneralSeperateItemDetails(String DriverExcelPath, String ItemDetailsSheetName, String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllOrderAttributes, String AllMessageAttributes, String AllShipToAddressAttributes, String AllBillToAddressAttributes, String AllBillToTaxAddressAttributes, String AllItemAttributes, String TestKeyword, String tcid) throws IOException {
		
		boolean res = false;
		
		//String ItemID[] = AllItemIDs.split(",");
		//String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
		String OrderAttributes[] = AllOrderAttributes.split(",");
		String MessageAttributes[] = AllMessageAttributes.split(",");
		String ShipToAddressAttributes[] = AllShipToAddressAttributes.split(",");
		String BillToAddressAttributes[] = AllBillToAddressAttributes.split(",");
		String BillToTaxAddressAttributes[] = AllBillToTaxAddressAttributes.split(",");
		//String ItemAttributes[] = AllItemAttributes.split(",");
		
		String ItemDetails = "";
		String Keyword = null;
		String Items = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    int r = 0;
	    int occurances = 0;
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
	        
	        httppost.setConfig(config);
	        
	        httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        /*for(int i = 1; i <= ItemID.length; i++) {
	        	
	        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\": " + i + ",\"itemBaseType\": \"" + ItemAttributes[0] + "\",\"itemAlternateId\": {\"skuMin\": \"" + ItemAttributes[1] + "\",\"skuPin\": \"" + ItemAttributes[2] + "\",\"skuLegacy\": \"" + ItemAttributes[3] + "\",\"barcodeAsin\": \"" + ItemAttributes[4] + "\",\"barcodeEan\": \"" + ItemAttributes[5] + "\"},\"itemDescription\": \"" + ItemAttributes[6] + "\",\"quantityType\": \"" + ItemAttributes[7] + "\",\"quantityOrdered\": " + QuantityOrdered[i-1] + ",\"priceOrderedCurrency\": \"" + ItemAttributes[8] + "\",\"priceOrderedAmount\": " + ItemAttributes[9] + ",\"priceOrderedTaxRate\": " + ItemAttributes[10] + "}";
	        	
	        	if(i!=ItemID.length)
	        		ItemDetails = ItemDetails + ",";
	        }*/
	        
	        Workbook wrk1 = Workbook.getWorkbook(new File(DriverExcelPath));
			
			Sheet sheet1 = wrk1.getSheet(ItemDetailsSheetName);
			
			int rows = sheet1.getRows();
			//int cols = sheet1.getColumns();
			
			for(r=1; r<rows; r++) {
				
				Keyword = sheet1.getCell(0, r).getContents().trim();
				
				if(occurances>0){
			          
			    	if(!(TestKeyword.contentEquals(Keyword))){

				    	break;
				    	
			    	}

				 }
				
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					  
					 occurances=occurances+1;
				
				Items = sheet1.getCell(1, r).getContents().trim();
				
				String[] AllItemsInItemDetails = Items.split(",");
				
				//System.out.println("Number of ItemIDs " + ItemID.length);
				System.out.println("Items with details " + AllItemsInItemDetails.length);
				
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					
					String ItemDetailsJson = sheet1.getCell(c, r).getContents().trim();
					
					ItemDetails = ItemDetails + ItemDetailsJson;
		        	
		        	if(c!=AllItemsInItemDetails.length + 1)
		        		ItemDetails = ItemDetails + ",";
				}
				}
				
			}
	        
	        //String json = "{ \"orders\": {\"orderId\": \"387429-02\", \"orderBuyerPartyId\": \"5013546056078\", \"orderSellerPartyId\": \"5013546229809\", \"orderReferenceCode\": \"Local-Order-3607-20-20170314\",\"messageId\": \"FS-20170811-133400\", \"messageSenderPartyId\": \"5013546056078\", \"messageRecipientPartyId\": \"5013546229809\", \"messageType\": \"MOR\", \"messageCreatedAt\": \"2017-08-11T13:34:00Z\",\"shipToPartyId\": \"MOR-HEEL\", \"shipToLocationId\": \"542\", \"shipToAddressName\": \"Palmer Harvey\", \"shipToAddressLine1\": \"Maxted Road\", \"shipToAddressLine2\": \"--\", \"shipToAddressCity\": \"Hemel Hempstead\", \"shipToAddressState\": \"Herts\", \"shipToAddressPostCode\": \"HP2 7DX\", \"shipToAddressCountryCode\": \"UK\",\"shipToDeliverAt\": \"2017-03-15\", \"shipToDeliverLatestAt\": \"2017-03-15\",\"billToPartyId\": \"5013546056078\", \"billToAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToAddressLine1\": \"P&H HOUSE\", \"billToAddressLine2\": \"DAVIGDOR ROAD\", \"billToAddressCity\": \"HOVE\", \"billToAddressState\": \"EAST SUSSEX\", \"billToAddressPostCode\": \"BN3 1RE\", \"billToAddressCountryCode\": \"UK\",\"billToTaxId\": \"GB232169089\", \"billToTaxAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToTaxAddressLine1\": \"P&H HOUSE\", \"billToTaxAddressLine2\": \"DAVIGDOR ROAD\", \"billToTaxAddressCity\": \"HOVE\", \"billToTaxAddressState\": \"EAST SUSSEX\", \"billToTaxAddressPostCode\": \"BN3 1RE\", \"billToTaxAddressCountryCode\": \"UK\"}, \"items\": [{\"itemId\": \"104466391\", \"itemLineId\": 1, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Pork Sausage Rolls 6s\",\"itemAlternateId\": { \"skuMin\": \"104466391\",\"skuLegacy\": \"0617707\", \"phId\": \"53238\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.75, \"priceOrderedTaxRate\": 0},{\"itemId\": \"105462210\", \"itemLineId\": 2, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Banana Flav Milk 1L\",\"itemAlternateId\": { \"skuMin\": \"105462210\",\"skuLegacy\": \"0403674\", \"phId\": \"53241\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.29, \"priceOrderedTaxRate\": 0}] }";
	       
	        //{\"itemId\": \"" + ItemID[0] + "\",\"itemLineId\": 1,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P104593867\",\"skuLegacy\": \"L0472851\",\"barcodeAsin\": \"ASIN29384845\",\"barcodeEan\": \"EAN3928445\"},\"itemDescription\": \"Heinz Baked Beans\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[0] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 1.00,\"priceOrderedTaxRate\": 20.00},{\"itemId\": \"" + ItemID[1] + "\",\"itemLineId\": 2,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P103201067\",\"skuLegacy\": \"L0419068\",\"barcodeAsin\": \"ASIN29383445\",\"barcodeEan\": \"EAN39234545\"},\"itemDescription\": \"Walkers Crips\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[1] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 2.00,\"priceOrderedTaxRate\": 5.00}
	        
	        
	        
	        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
	        String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";
	        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

	        
	        // httppost.setEntity(new StringEntity(json, "UTF-8"));
	        
	        System.out.println(json);
	        
	        httppost.setEntity(new StringEntity(json));
			
	       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/json");
			
			utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
			response = httpclient.execute(target, httppost);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode());

	        System.out.println(EntityUtils.toString(response.getEntity()));
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()));
	    
	        //response.toString();
			if(response.getStatusLine().getStatusCode() == 201)
				res = true;
			else
				res=false;
			
	        
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
	        
			res = false;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
	
	
public static boolean PostCallOrderCreationGeneralSeperateItemDetails_Latest(String DriverExcelPath, String ItemDetailsSheetName, String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String OrderCount,String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllOrderAttributes, String AllMessageAttributes, String AllShipToAddressAttributes, String AllBillToAddressAttributes, String AllBillToTaxAddressAttributes, String AllItemAttributes, String TestKeyword, String tcid) throws IOException {
		
		boolean res = false;
		
		//String OrderCount="1:1";
		
		//String ItemID[] = AllItemIDs.split(",");
		//String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
		String OrderAttributes[] = AllOrderAttributes.split(",");
		String MessageAttributes[] = AllMessageAttributes.split(",");
		String ShipToAddressAttributes[] = AllShipToAddressAttributes.split(",");
		String BillToAddressAttributes[] = AllBillToAddressAttributes.split(",");
		String BillToTaxAddressAttributes[] = AllBillToTaxAddressAttributes.split(",");
		//String ItemAttributes[] = AllItemAttributes.split(",");
		
		String ItemDetails = "";
		String Keyword = null;
		String Items = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    int r = 0;
	    int occurances = 0;
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
	        
	        httppost.setConfig(config);
	        
	        httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        /*for(int i = 1; i <= ItemID.length; i++) {
	        	
	        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\": " + i + ",\"itemBaseType\": \"" + ItemAttributes[0] + "\",\"itemAlternateId\": {\"skuMin\": \"" + ItemAttributes[1] + "\",\"skuPin\": \"" + ItemAttributes[2] + "\",\"skuLegacy\": \"" + ItemAttributes[3] + "\",\"barcodeAsin\": \"" + ItemAttributes[4] + "\",\"barcodeEan\": \"" + ItemAttributes[5] + "\"},\"itemDescription\": \"" + ItemAttributes[6] + "\",\"quantityType\": \"" + ItemAttributes[7] + "\",\"quantityOrdered\": " + QuantityOrdered[i-1] + ",\"priceOrderedCurrency\": \"" + ItemAttributes[8] + "\",\"priceOrderedAmount\": " + ItemAttributes[9] + ",\"priceOrderedTaxRate\": " + ItemAttributes[10] + "}";
	        	
	        	if(i!=ItemID.length)
	        		ItemDetails = ItemDetails + ",";
	        }*/
	        
	        Workbook wrk1 = Workbook.getWorkbook(new File(DriverExcelPath));
			
			Sheet sheet1 = wrk1.getSheet(ItemDetailsSheetName);
			
			int rows = sheet1.getRows();
			//int cols = sheet1.getColumns();
			
			for(r=1; r<rows; r++) {
				
				Keyword = sheet1.getCell(0, r).getContents().trim();
				
				if(occurances>0){
			          
			    	if(!(TestKeyword.contentEquals(Keyword))){

				    	break;
				    	
			    	}

				 }
				
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					  
					 occurances=occurances+1;
				
				Items = sheet1.getCell(1, r).getContents().trim();
				
				String[] AllItemsInItemDetails = Items.split(",");
				
				//System.out.println("Number of ItemIDs " + ItemID.length);
				System.out.println("Items with details " + AllItemsInItemDetails.length);
				
				for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
					
					String ItemDetailsJson = sheet1.getCell(c, r).getContents().trim();
					
					ItemDetails = ItemDetails + ItemDetailsJson;
		        	
		        	if(c!=AllItemsInItemDetails.length + 1)
		        		ItemDetails = ItemDetails + ",";
				}
				}
				
			}
	        
	        //String json = "{ \"orders\": {\"orderId\": \"387429-02\", \"orderBuyerPartyId\": \"5013546056078\", \"orderSellerPartyId\": \"5013546229809\", \"orderReferenceCode\": \"Local-Order-3607-20-20170314\",\"messageId\": \"FS-20170811-133400\", \"messageSenderPartyId\": \"5013546056078\", \"messageRecipientPartyId\": \"5013546229809\", \"messageType\": \"MOR\", \"messageCreatedAt\": \"2017-08-11T13:34:00Z\",\"shipToPartyId\": \"MOR-HEEL\", \"shipToLocationId\": \"542\", \"shipToAddressName\": \"Palmer Harvey\", \"shipToAddressLine1\": \"Maxted Road\", \"shipToAddressLine2\": \"--\", \"shipToAddressCity\": \"Hemel Hempstead\", \"shipToAddressState\": \"Herts\", \"shipToAddressPostCode\": \"HP2 7DX\", \"shipToAddressCountryCode\": \"UK\",\"shipToDeliverAt\": \"2017-03-15\", \"shipToDeliverLatestAt\": \"2017-03-15\",\"billToPartyId\": \"5013546056078\", \"billToAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToAddressLine1\": \"P&H HOUSE\", \"billToAddressLine2\": \"DAVIGDOR ROAD\", \"billToAddressCity\": \"HOVE\", \"billToAddressState\": \"EAST SUSSEX\", \"billToAddressPostCode\": \"BN3 1RE\", \"billToAddressCountryCode\": \"UK\",\"billToTaxId\": \"GB232169089\", \"billToTaxAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToTaxAddressLine1\": \"P&H HOUSE\", \"billToTaxAddressLine2\": \"DAVIGDOR ROAD\", \"billToTaxAddressCity\": \"HOVE\", \"billToTaxAddressState\": \"EAST SUSSEX\", \"billToTaxAddressPostCode\": \"BN3 1RE\", \"billToTaxAddressCountryCode\": \"UK\"}, \"items\": [{\"itemId\": \"104466391\", \"itemLineId\": 1, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Pork Sausage Rolls 6s\",\"itemAlternateId\": { \"skuMin\": \"104466391\",\"skuLegacy\": \"0617707\", \"phId\": \"53238\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.75, \"priceOrderedTaxRate\": 0},{\"itemId\": \"105462210\", \"itemLineId\": 2, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Banana Flav Milk 1L\",\"itemAlternateId\": { \"skuMin\": \"105462210\",\"skuLegacy\": \"0403674\", \"phId\": \"53241\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.29, \"priceOrderedTaxRate\": 0}] }";
	       
	        //{\"itemId\": \"" + ItemID[0] + "\",\"itemLineId\": 1,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P104593867\",\"skuLegacy\": \"L0472851\",\"barcodeAsin\": \"ASIN29384845\",\"barcodeEan\": \"EAN3928445\"},\"itemDescription\": \"Heinz Baked Beans\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[0] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 1.00,\"priceOrderedTaxRate\": 20.00},{\"itemId\": \"" + ItemID[1] + "\",\"itemLineId\": 2,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P103201067\",\"skuLegacy\": \"L0419068\",\"barcodeAsin\": \"ASIN29383445\",\"barcodeEan\": \"EAN39234545\"},\"itemDescription\": \"Walkers Crips\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[1] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 2.00,\"priceOrderedTaxRate\": 5.00}
	        
	        
	        
	        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
	        String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderCount\": \"" + OrderCount + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";
	       
	        
	        
	        
	        
	        
	        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

	        
	        // httppost.setEntity(new StringEntity(json, "UTF-8"));
	        
	        System.out.println(json);
	        
	        httppost.setEntity(new StringEntity(json));
			
	       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/json");
			
			utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
			response = httpclient.execute(target, httppost);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode());

	        System.out.println(EntityUtils.toString(response.getEntity()));
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()));
	    
	        //response.toString();
			if(response.getStatusLine().getStatusCode() == 201)
				res = true;
			else
				res=false;
			
	        
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
	        
			res = false;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
	
	
	
	
	
	
	
	

}
