package Utilities;

import java.io.BufferedInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;

import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.jcraft.jsch.JSchException;
import com.relevantcodes.extentreports.ExtentTest;

public class RMS_DB {
	
	
	
	
	public static String ValidateStagingTable(String ItemID, String Location, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

		String res=null;
		

	   try{    
		   		String nasres=null;

				String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
				
				Connection conn = DriverManager.getConnection(connectionURL, username, password);
				System.out.println("Connected");
				PreparedStatement ps= conn.prepareStatement("select X_MESSAGE_ID, adj_qty,adj_date, ADJ_PROC_STATUS, Created from RMSPRD.XXWMM_MOR_STKADJ_INBOUND where location in ('"+Location+"') and item in ('"+ItemID+"') and rownum=1 order by Created");
				utilityFileWriteOP.writeToLog(tcid, "Query for tran table: ","select X_MESSAGE_ID , adj_qty,adj_date from RMSPRD.XXWMM_MOR_STKADJ_INBOUND where location in ('"+Location+"') and item in ('"+ItemID+"') and rownum=1 order by adj_date;",resultpath);
				ResultSet rs = ps.executeQuery();
				  String adj_qty=null;
				  String adj_date = null;
				  String msg_id=null;
				  String status=null;
				while(rs.next()){
					
					msg_id = rs.getString("X_MESSAGE_ID");
					  adj_qty=rs.getString("adj_qty");
					  adj_date=rs.getString("adj_date");
					  status=rs.getString("ADJ_PROC_STATUS");
				}
				int pass_count=0;
				
				if(!(msg_id.equalsIgnoreCase(null)))
				{pass_count++;
					utilityFileWriteOP.writeToLog(tcid,"msg_id is :"+msg_id," ", resultpath);
				}
				if(!(status.equalsIgnoreCase(null)))
				{pass_count++;
					utilityFileWriteOP.writeToLog(tcid,"status is :"+status," ", resultpath);
				}
				
				if(pass_count==2)
				{
					res=status+","+adj_qty;
				}
		
				System.out.println("X_MESSAGE_ID of item is: "+res);
				utilityFileWriteOP.writeToLog(tcid,"X_MESSAGE_ID of item is: "+res," ", resultpath);
				utilityFileWriteOP.writeToLog(tcid,"adj_qty of item is :"+adj_qty," ", resultpath);
				utilityFileWriteOP.writeToLog(tcid,"adj_date of item is :"+adj_date," ", resultpath);
				
				}catch(Exception e){
					e.printStackTrace();
					//System.out.println(e);
					res=null;
					utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms", "Fail", resultpath);
					   } 


					finally{        

						
					 return res;
					}
					
			}
	
	public static String GetEanFromRMS(String ItemID , String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

		String res=null;
		String result = null;

	   try{    
		   		String nasres=null;

				String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
				
				Connection conn = DriverManager.getConnection(connectionURL, username, password);
				
				System.out.println("Connected");
				
				
				
				
				PreparedStatement ps= conn.prepareStatement("select item from RMSPRD.item_master where ITEM_PARENT='"+ItemID+"'");
				
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					
					  result = rs.getString("item");
				
				}
				
				 res=result.trim();
				 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
				
			
				
				
				}catch(Exception e){
					e.printStackTrace();
					//System.out.println(e);
					res=null;
					utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing item_loc_soh table from RDS"," ", resultpath);
					   } 


					finally{        

						
					 return res;
					}
					
				}


public static String GetDSD_INDFromRMSDD(String externalsiteIDFromRMS, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	String res=null;
	String result = null;

   try{    

		System.out.println("Before Connected");
		//System.out.println(hostname);
		//System.out.println(username);
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			
	
			System.out.println("Connected");
			
			
			
			PreparedStatement ps= conn.prepareStatement("select DSD_IND from RMSPRD.sups where supplier='"+externalsiteIDFromRMS+"'");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				  result = rs.getString("DSD_IND");
			
			}
			
			 res=result.trim();
			 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
			
		
			
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing item_loc_soh table from RDS"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
			}



public static String GetExternalSITEIDFromRMS(String ITEM_ID, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	String res=null;
	String result = null;

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select supplier from RMSPRD.item_supp_country where item='"+ITEM_ID+"'");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				  result = rs.getString("supplier");
			
			}
			
			 res=result.trim();
			 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
			
		
			
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing item_loc_soh table from RDS"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
			}
public static String GetExternalSITEIDForItemID(String ITEM_ID,String externalsiteIDFromRMS, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	String res=null;
	String result = null;

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			
			System.out.println("Connected");
			
			
			
			
			PreparedStatement ps= conn.prepareStatement("select supplier from RMSPRD.item_supp_country where item='"+ITEM_ID+"'and supplier='"+externalsiteIDFromRMS+"'");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				  result = rs.getString("supplier");
			
			}
			
			 res=result.trim();
			 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
			
		
			
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing item_loc_soh table from RDS"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
			}

public static String GetStockOnHand(String ITEM_ID,String Location,String externalsiteIDFromRMS, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	String res=null;
	String result = null;

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			
			System.out.println("Connected");
			
			
			
			
			PreparedStatement ps= conn.prepareStatement("select Stock_on_hand from RMSPRD.item_loc_soh where item='"+ITEM_ID+"' and loc in("+Location+")");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				  result = rs.getString("Stock_on_hand");
			
			}
			System.out.println(result);
			 res=result.trim();
			 utilityFileWriteOP.writeToLog(tcid, "Stock on Hand for item: "+ITEM_ID+" is"+res,"", resultpath); 
			
		
			
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing item_loc_soh table from RDS"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
			}



public static boolean ValidateOrdHeadtable(String OrderId,String Supplier,String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	boolean res=false;
	

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select order_no from RMSPRD.ordhead where order_no='5"+OrderId+"' and supplier='"+Supplier+"'");
			
			ResultSet rs = ps.executeQuery();
			
		
			String ColumnName="";
			 ResultSetMetaData metadata = rs.getMetaData();
			    int columnCount = metadata.getColumnCount();  
			    if(columnCount!=0)
			    {
			    	res=true;
			    }
			    for (int i = 1; i <= columnCount; i++) {
			        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
			    }
			    String row = "";
			    while (rs.next()) {
			        
			        for (int i = 1; i <= columnCount; i++) {
			            row += rs.getString(i) + ", ";          
			        }
			       
			    }

			    
		
			 utilityFileWriteOP.writeToLog(tcid, "Column names are:", ColumnName,resultpath);
			 utilityFileWriteOP.writeToLog(tcid, "Row Values are",row,resultpath);
		
			System.out.println(ColumnName);
			System.out.println(row);
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=false;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
			}




public static boolean ValidateOrdlocTable(String OrderId,String ItemID,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	boolean res=false;
	

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select UNIT_RETAIL,QTY_ORDERED,QTY_PRESCALED,QTY_RECEIVED from RMSPRD.ordloc where order_no='5"+OrderId+"' and item='"+ItemID+"'");
			
			ResultSet rs = ps.executeQuery();
			
		
			String ColumnName="";
			 ResultSetMetaData metadata = rs.getMetaData();
			    int columnCount = metadata.getColumnCount();  
			    if(columnCount!=0)
			    {
			    	res=true;
			    }
			    for (int i = 1; i <= columnCount; i++) {
			        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
			    }
			    String row = "";
			    while (rs.next()) {
			        
			        for (int i = 1; i <= columnCount; i++) {
			            row += rs.getString(i) + ", ";          
			        }
			       
			    }

			    
		
			 utilityFileWriteOP.writeToLog(tcid, "Column names are:", ColumnName,resultpath);
			 utilityFileWriteOP.writeToLog(tcid, "Row Values are",row,resultpath);
		
			System.out.println(ColumnName);
			System.out.println(row);
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=false;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
   
}
   
   public static String ValidateItemLocTable(String ItemID,String Location,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

		String res=null;
		

	   try{    
		   		String nasres=null;

				String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
				
				Connection conn = DriverManager.getConnection(connectionURL, username, password);
				System.out.println("Connected");
				PreparedStatement ps= conn.prepareStatement("select item,loc,stock_on_hand,qty_received from RMSPRD.item_loc_soh where loc in("+Location+") and item='"+ItemID+"'");
				utilityFileWriteOP.writeToLog(tcid, "Query for item loc table is:","select item,loc,stock_on_hand,qty_received from item_loc_soh where loc in("+Location+") and item='"+ItemID+"'",resultpath);
				ResultSet rs = ps.executeQuery();
				
			
				String ColumnName="";
				 ResultSetMetaData metadata = rs.getMetaData();
				    int columnCount = metadata.getColumnCount();  
				 
				    for (int i = 1; i <= columnCount; i++) {
				        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
				    }
				    String row = "";
				    while (rs.next()) {
				        
				        for (int i = 1; i <= columnCount; i++) {
				            row += rs.getString(i) + ", ";          
				        }
				       
				    }

				    String rows[]=row.split(",");
			
	
				 utilityFileWriteOP.writeToLog(tcid, "Min of item is:",rows[0],resultpath);
				 utilityFileWriteOP.writeToLog(tcid, "loc is:",rows[1],resultpath);
				 utilityFileWriteOP.writeToLog(tcid, "Stock_on_hand is:",rows[2],resultpath);
				 utilityFileWriteOP.writeToLog(tcid, "qty_received is:",rows[3],resultpath);
			System.out.println("Min of item is:"+rows[0]);
			System.out.println("loc is:"+rows[1]);
			System.out.println("Stock_on_hand is:"+rows[2]);
			System.out.println("qty_received is:"+rows[3]);
			res=rows[2].trim();
			
				}catch(Exception e){
					e.printStackTrace();
					//System.out.println(e);
					res=null;
					utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
					   } 


					finally{        

						
					 return res;
					}
					
			}
   
   
   public static boolean ValidatetranDataTable(String ItemID,String TransferId,String Location,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

		boolean res=false;
		

	   try{    
		   		String nasres=null;

				String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
				
				Connection conn = DriverManager.getConnection(connectionURL, username, password);
				System.out.println("Connected");
				PreparedStatement ps= conn.prepareStatement("select item,location,tran_code,tran_date,loc_type,units,ref_no_1, adj_code from RMSPRD.tran_data where location in("+Location+") and item='"+ItemID+"' and ref_no_1='"+TransferId+"'");
				utilityFileWriteOP.writeToLog(tcid, "Query for tran table: ","select item,location,tran_code,tran_date,loc_type,units,ref_no_1 from RMSPRD.tran_data where location in("+Location+") and item='"+ItemID+"' and ref_no_1='"+TransferId+"'",resultpath);
				ResultSet rs = ps.executeQuery();
				
			
				String ColumnName="";
				 ResultSetMetaData metadata = rs.getMetaData();
				    int columnCount = metadata.getColumnCount();  
				    if(columnCount!=0)
				    {
				    	res=true;
				    }
				    for (int i = 1; i <= columnCount; i++) {
				        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
				    }
				    String row = "";
				    while (rs.next()) {
				        
				        for (int i = 1; i <= columnCount; i++) {
				            row += rs.getString(i) + ", ";          
				        }
				       
				    }

		String 	rows[]=row.split(",");
		utilityFileWriteOP.writeToLog(tcid, "Min of item is :",rows[0],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "Location of item is :",rows[1],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "tran_code of item is :",rows[2],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "tran_date of item is :",rows[3],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "loc_type of item is :",rows[4],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "units of item is :",rows[5],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "ref_no_1 of item is :",rows[6],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "adj_code of item is :",rows[7],resultpath);
				System.out.println( "Min of item is :"+rows[0]);
				System.out.println("Location of item is :"+rows[1]);
				System.out.println("tran_code of item is :"+rows[2]);
				System.out.println("tran_date of item is :"+rows[3]);
				System.out.println("loc_type of item is :"+rows[4]);
				System.out.println("units of item is :"+rows[5]);
				System.out.println("ref_no_1 of item is :"+rows[6]);
				System.out.println("adj_code of item is :"+rows[7]);
				}catch(Exception e){
					e.printStackTrace();
					//System.out.println(e);
					res=false;
					utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
					   } 


					finally{        

						
					 return res;
					}
					
			}


public static boolean ValidatetsfHead(String TransferId,String OrderID,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	boolean res=false;
	

	   try{   

			String from_loc=null;
			String to_loc=null;
			int loop_Count=0;
		   do
	   				{
			   		loop_Count++;
			   		if( loop_Count>2)
			   		{
			   			break;
			   		}
		   		String nasres=null;

				String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
				
				Connection conn = DriverManager.getConnection(connectionURL, username, password);
				System.out.println("connected");
				
				PreparedStatement ps= conn.prepareStatement("select FROM_LOC,TO_LOC from RMSPRD.tsfhead where TSF_NO='"+TransferId+"'");
				 utilityFileWriteOP.writeToLog(tcid, "Query for tsf header table: ","select FROM_LOC,TO_LOC from RMSPRD.tsfhead where TSF_NO='"+TransferId+"'",resultpath);
				Thread.sleep(2000);
				
				 ResultSet rs = ps.executeQuery();
			
				
				while(rs.next())
				{
					from_loc=rs.getString("FROM_LOC");
					to_loc=rs.getString("TO_LOC");
				}
	   				}
		   			while(from_loc.equalsIgnoreCase(null) );
				
					if(from_loc!=null && to_loc!=null)
					{
						res=true;
					}else
					{
						res=false;
					}
						
					System.out.println("From loc of item"+from_loc);
					System.out.println("To loc of item"+to_loc);
				 utilityFileWriteOP.writeToLog(tcid, "From loc of item",from_loc,resultpath);
				 utilityFileWriteOP.writeToLog(tcid, "To loc of item",to_loc,resultpath);
				 
				
				
				}catch(Exception e){
					e.printStackTrace();
					//System.out.println(e);
					res=false;
					utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
					   } 


					finally{        

						
					 return res;
					}
				
		}


public static boolean ValidatetsfDetails(String TransferId,String OrderID,String ITEMID,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	boolean res=false;
	

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select ITEM,tsf_qty,ship_qty,received_qty from RMSPRD.tsfdetail  where TSF_NO='"+TransferId+"' and ITEM='"+ITEMID+"'");
			utilityFileWriteOP.writeToLog(tcid, "Query for tsf  details table:","select ITEM,tsf_qty,ship_qty,received_qty from tsfdetail  where TSF_NO='"+TransferId+"'",resultpath);
			ResultSet rs = ps.executeQuery();
			
		
			String ColumnName="";
			 ResultSetMetaData metadata = rs.getMetaData();
			    int columnCount = metadata.getColumnCount();  
			    if(columnCount!=0)
			    {
			    	res=true;
			    }
			    for (int i = 1; i <= columnCount; i++) {
			        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
			    }
			    String row = "";
			    while (rs.next()) {
			        
			        for (int i = 1; i <= columnCount; i++) {
			            row += rs.getString(i) + ", ";          
			        }
			       
			    }

			    
		
			
		String rows[]=row.split(",");
		utilityFileWriteOP.writeToLog(tcid, "Item min is:",rows[0],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "tsf quantity is:",rows[1],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "ship quantity is:",rows[2],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "received quantity is:",rows[3],resultpath);
		System.out.println("Item min is:"+rows[0]);
		System.out.println("tsf quantity is:"+rows[1]);
		System.out.println("ship quantity is:"+rows[2]);
		System.out.println("received quantity is:"+rows[3]);
		if(rows[2].equalsIgnoreCase(rows[3]))
		{
			 utilityFileWriteOP.writeToLog(tcid, "Shipped qty is equal to received qty",rows[rows.length-2],resultpath);
			
		}
			System.out.println(ColumnName);
			System.out.println(row);
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=false;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
		}
public static boolean ValidateShipmentTable(String Shipment,String OrderID,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	boolean res=false;
	

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select Status_code from RMSPRD.shipment  where Shipment='"+Shipment+"'");
			 utilityFileWriteOP.writeToLog(tcid, "Query of shipment table:","select Status_code from shipment  where Shipment='"+Shipment+"'",resultpath);
			ResultSet rs = ps.executeQuery();
			
		
			String ColumnName="";
			 ResultSetMetaData metadata = rs.getMetaData();
			    int columnCount = metadata.getColumnCount();  
			    if(columnCount!=0)
			    {
			    	res=true;
			    }
			    for (int i = 1; i <= columnCount; i++) {
			        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
			    }
			    String row = "";
			    while (rs.next()) {
			        
			        for (int i = 1; i <= columnCount; i++) {
			            row += rs.getString(i) + ", ";          
			        }
			       
			    }
			    String rows[]=row.split(",");
			 utilityFileWriteOP.writeToLog(tcid, "Status code is:",rows[0],resultpath);
		
		if(row.trim().equalsIgnoreCase("R"))
		{
			 utilityFileWriteOP.writeToLog(tcid, "Status code is R","PASS",resultpath);
		}
			System.out.println(ColumnName);
			System.out.println(row);
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=false;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
		}
public static String  Validateshipsku(String TransferId,String OrderID,String ItemID,String Supplier, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun) throws JSchException,NullPointerException{

	String res="";
	

   try{    
	   		String nasres=null;

			String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement("select ITEM,shipment,qty_received,qty_expected from RMSPRD.shipsku  where distro_no='"+TransferId+"' and ITEM='"+ItemID+"'");
			 utilityFileWriteOP.writeToLog(tcid, "Query for shipsku table:","select ITEM,shipment,qty_received,qty_expected from shipsku  where distro_no='"+TransferId+"'",resultpath);
			ResultSet rs = ps.executeQuery();
			
		
			String ColumnName="";
			 ResultSetMetaData metadata = rs.getMetaData();
			    int columnCount = metadata.getColumnCount();  
			    if(columnCount!=0)
			    {
			   
			    }
			    for (int i = 1; i <= columnCount; i++) {
			        ColumnName=ColumnName+metadata.getColumnName(i).trim()+",";      
			    }
			    String row = "";
			    while (rs.next()) {
			        
			        for (int i = 1; i <= columnCount; i++) {
			            row += rs.getString(i) + ", ";          
			        }
			       
			    }

			  
		
			
		String rows[]=row.split(",");
		res=rows[1];
		utilityFileWriteOP.writeToLog(tcid, "Min of item is:",rows[0],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "Shipment id is:",rows[1],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "Qty_received of item is:",rows[2],resultpath);
		utilityFileWriteOP.writeToLog(tcid, "Qty_expected of item is:",rows[3],resultpath);
		System.out.println("Min of item is:"+rows[0]);
		System.out.println("Shipment id is:"+rows[1]);
		System.out.println("Qty_received of item is:"+rows[2]);
		System.out.println("Qty_expected of item is:"+rows[3]);
		
		if(rows[2].equalsIgnoreCase(rows[3]))
		{
			 utilityFileWriteOP.writeToLog(tcid, "qty received is equal to  qty expected",rows[rows.length-2],resultpath);
			
		}
		
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing orderloc table from Rms"," ", resultpath);
				   } 


				finally{        

					
				 return res;
				}
				
		}


public static HashMap<String, String> FetchSOHFromRMS(String itemids,String Wh, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath) throws JSchException,NullPointerException{

	//String res=null;
	HashMap<String, String> res= new HashMap<String, String>();
	String result = null;
	BufferedInputStream buffer;

	String [] itemList=itemids.split(",");
	
	String concatinate=null;

	
	
	int c=0;
	
	
	for(int i=0;i<itemList.length;i++){
		
		
	//	allItems="'"+itemList[i]+"'";
		
    if(c==0){
			
		concatinate="'"+itemList[i]+"'";	
		}
		
		else{
		
		concatinate=concatinate+","+"'"+itemList[i]+"'";
		
		
		}
		
		c=c+1;
		
	}
	

   try{    
	   		String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			
			
			System.out.println("Connected");
			
		//	String SQL = "select stock_on_hand from  item_loc_soh where item='"+itemids+"' and loc in ('"+Wh+"')";
			
			String SQL = "select stock_on_hand from  item_loc_soh where item in ("+concatinate+" ) and loc in ('"+Wh+"')";
			
			System.out.println(SQL);
			
			
			PreparedStatement ps= conn.prepareStatement(SQL);
			
			
			
			ResultSet rs = ps.executeQuery();
	

			
			int recCount=0;
			
			
			while(rs.next()){
				
		
						
			System.out.println("Within while....");
			String  itemQty=rs.getString("stock_on_hand");
			
			System.out.println("Item No :"+itemList[recCount]+"Quantity is "+itemQty);

			res.put(itemList[recCount], itemQty);
			
			
			recCount=recCount+1;
					
			
			}	

		
			
			
			}
   
   
   
   catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLogUFT("Error occured During accessing RMS", resultpath);
				
				System.out.println("Within Catch ....");
				
   }	
				
			
   finally{
   
   return res;
   
   }
				   
			
} 



public static HashMap<String, String> GetDetailsFromRMS(String SQL, String fieldsRetrieved, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun, ExtentTest logger) throws JSchException,NullPointerException{

//	String res=null;
	HashMap<String, String> res= new HashMap<String, String>();
	String result = null;
	BufferedInputStream buffer;
	String[] fieldsArray=fieldsRetrieved.split(",");

   try{    
	   		String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement(SQL);
			ResultSet rs = ps.executeQuery();
			utilityFileWriteOP.writeToLog(tcid,"Query Result",rs.toString(), resultpath);
//			utilityFileWriteOP.writeToLog(tcid,"Query Result",rs.toString(), resultpath,xwpfRun);
			
			while(rs.next()){
				 
				for(int i=0;i<fieldsArray.length;i++){
					result=null;
					result = rs.getString(fieldsArray[i]);
					if(result!=null){
						res.put(fieldsArray[i], result.trim());
						utilityFileWriteOP.writeToLog(tcid,"Query Result",fieldsArray[i]+" : "+result.trim(), resultpath);
					}
				}
			
			}
			
			 
			 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
			
		
			 return res;
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=null;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing RMS"," ", resultpath);
				return res;
				   } 


				
				
			}

public static boolean VerifyTableifEmptyRMS(String SQL, String fieldsRetrieved, String hostname, String port, String username, String password,String service_name, String tcid,String TestDataPath, String resultpath,XWPFRun xwpfRun, ExtentTest logger) throws JSchException,NullPointerException{

//	String res=null;
//	HashMap<String, String> res= new HashMap<String, String>();
	boolean res=false;
	String result = null;
	String[] fieldsArray=fieldsRetrieved.split(",");

   try{    
	   		String connectionURL = "jdbc:oracle:thin:@"+hostname+":"+port+":"+service_name+"";
			Connection conn = DriverManager.getConnection(connectionURL, username, password);
			System.out.println("Connected");
			PreparedStatement ps= conn.prepareStatement(SQL);
			ResultSet rs = ps.executeQuery();
//			utilityFileWriteOP.writeToLog(tcid,"Query Result",rs.toString(), resultpath);
//			utilityFileWriteOP.writeToLog(tcid,"Query Result",rs.toString(), resultpath,xwpfRun);
			
			res=rs.next();
			
			 
			 //utilityFileWriteOP.writeToLog(tcid, "Query Result for Tran Data","", resultpath); 
			
		
			 return res;
			
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
				res=false;
				utilityFileWriteOP.writeToLog(tcid,"Error occured During accessing RMS"," ", resultpath);
				return res;
				   } 


				
				
			}

}

