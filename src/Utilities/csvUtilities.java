package Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class csvUtilities {
	
	public static boolean RetrieveSinglefieldDetailscsv(String downloadpath, String filename,String ExpectedValue, String Resultpath, String tcid, XWPFRun xwpfrun) throws Exception {
			boolean res=false;
			
			try{
				
				//Retrievefilename
				
				if(filename==null){
					
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				
				File file = new File(downloadpath+filename);
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String line, header;
				String[] headers=br.readLine().split(","); // consume first line and ignore
				line = br.readLine();
				
				if(line.trim().contains(ExpectedValue))
					res=true;
				else{
					res=false;
					System.setProperty("eventlog", line);
				}
		        br.close();
		    	return res;
			} catch (Exception e) {
				res=false;
				e.printStackTrace();
				utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath,xwpfrun);
				utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
				return res;	
			}
			
		}
	
	private static Map<String, Boolean> areEqualKeyValues(Map<String, String> first, Map<String, String> second) {
	    return first.entrySet().stream()
	      .collect(Collectors.toMap(e -> e.getKey(), 
	        e -> e.getValue().equals(second.get(e.getKey()))));
	}
	
	public static boolean whenCompareTwoHashMapKeyValuesUsingStreamAPI_thenSuccess(Map<String, String> source,Map<String, String> destination, String tablename, ExtentTest logger) {
	    boolean res=false;   
		Map<String, Boolean> result = areEqualKeyValues(source, destination);
	       Iterator<Entry<String, Boolean>> it = result.entrySet().iterator();
		    while (it.hasNext()) {
		        Entry<String, Boolean> pair = it.next();
//		        System.out.println(pair.getKey() + " = " + pair.getValue());
		        if(result.get(pair.getKey())){
		        	if(source.get(pair.getKey()).contains(destination.get(pair.getKey()))){
		        		logger.log(LogStatus.PASS, "Validation in "+tablename+" table is successful for the field "+pair.getKey()+": "+result.get(pair.getKey()));
			        	res= true;
		        	}else{
		        		logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful for the field "+pair.getKey()+": "+result.get(pair.getKey()));
//			        	res=false;
		        	}
		        	
		        }else{
//		        	logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful for the field "+pair.getKey()+": "+result.get(pair.getKey()));
//		        	res=false;
//		        	break;
		        }
		        it.remove(); // avoids a ConcurrentModificationException
		    }
		    return res;
	}
}
