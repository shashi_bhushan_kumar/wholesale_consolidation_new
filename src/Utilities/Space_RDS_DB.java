package Utilities;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;

//import Utilities.RDS_walkitem;
import Utilities.excelCellValueWrite;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import com.jcraft.jsch.*;

public class Space_RDS_DB {
	
	
	
	
	public static boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	       
	        i = 0;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}
	
	
	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}

	
	
	
	public static boolean isStringNumeric( String str )
	{
		boolean isNumeric = str.chars().allMatch( Character::isDigit );
		
		return isNumeric;
	}
	
	
	
	public static Boolean getPlanNamesRDS(String min,String loc,String planName,Integer r,String DriverSheetPath,String SheetName) throws JSchException{
	        	
	   		 Channel channel=null;
	   		 Session session=null;
	   	     
	   	     boolean res=false;    

	   	        try{      
	   	              
	   	              JSch jsch = new JSch();
	   	              
	   	              java.util.Properties config = new java.util.Properties(); 
	   	              

	   	              String command = "psql --host=space-nonprod-space-rds-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_space_v1_db_sit --user tcs_testing;";
	   	       
	   	              
	   	              
	   	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	   	              
	   	             config.put("StrictHostKeyChecking", "no");
	   	              
	   	              session.setConfig(config);
	   	              
	   	             
	   	              
	   	          session.setPassword("2Bchanged");
	   	          session.connect();
	                 
	                // System.out.println("Connection success");

	                 channel = session.openChannel("exec");
	                 ((ChannelExec)channel).setCommand(command);

	                 channel.setInputStream(null);
	                 ((ChannelExec)channel).setErrStream(System.err);
	                 InputStream in = channel.getInputStream();
	               ((ChannelExec)channel).setPty(false);
	                 
	                 OutputStream out=channel.getOutputStream();
	                 channel.connect();
	                 
	                 Thread.sleep(1000);
	   	              
	   	        out.write(("ed1f64006038"+"\n").getBytes());
	   	        out.flush();
	   	        
	   	        Thread.sleep(1000);

	   	    // out.write(("Select plan_name, min, store_number, space_type,total_fill_quantity ,facings,units_high,units_deep,fill_quantity,shelf,position  from space where store_number = "+loc+" and min ='"+min+"';"+"\n").getBytes());

	   	     out.write(("Select plan_name from space where store_number = "+loc+" and min ='"+min+"';"+"\n").getBytes());
	   	        out.flush();
	   	        
	   	        Thread.sleep(1000);
	   	        
	   	       
	   	        
	   	        byte[] tmp = new byte[10240];
	   	           
	   	        int i=0;  
	   	              
	   	        
	   	          while (in.available() > 0) {
	   	                	
	   	                	{
	   	                  
	   	                i = in.read(tmp, 0, 10240);
	   	                   
	   	                    if (i < 0) {
	   	                        break;
	   	 
	   	                    }
	   	                  
	   	                }  
	   	                	
	   	                	
	   	               
	   	               }
	   	          
	   	        String QResult=new String(tmp, 0, i); 
	   	        
	   	       // System.out.println(QResult);
	   	           
	   	        
	   	        String [] line=   QResult.split("\n");
	   	        
	   	   
	   	     for(int j=0;j<line.length;j++)
		   	   {
		   		   if (line[j].replaceAll("\\|", "").trim().contentEquals("(0 rows)")||(line[j].replaceAll("\\|", "").trim().contentEquals("")) ) 
		   		   {
		   			   res=true;
		   			   
		   			   return res;
		   		   }
		   	   }      
	   	        
	   	     
	   	        String [] line1=   QResult.split("plan_name");

		        String [] line2=   line1[1].split("\n");

		 	   
	   	        
	   	ArrayList<String> words = new ArrayList<String>();
	   	        
	   	
	   	
	   	   for(int j=2;j<line2.length-2;j++)
	   	   {
	   		words.add(line2[j].replaceAll("\\|", "").trim());
	   		   

	   	   }
	   	// System.out.println("Plans: "+words);
	   	 
	   	 
	   	 if (!(words.isEmpty())) 
	   	 {
		
	   		 res=true;
		}
	   	 
	   	
	     for (int j = 0; j < words.size(); j++) 
		   {
			   if (!(planName.isEmpty()))
			   {
			
				   planName=planName+","+words.get(j).trim();
			   
			   }
			   else
			   {
				   planName=words.get(j).trim();
				   
			   }
			   
		   }
	     
	     System.out.println("Plan Names: "+planName);
	   	 
	   	 
	   	 excelCellValueWrite.writeValueToCell(planName, r, 11, DriverSheetPath,SheetName);
	   	   

	   	  channel.disconnect();

	   	  session.disconnect();
	   	  
	   	  Thread.sleep(5000);
	   	  
	   	  out.close();
	   	  
	   	  in.close();
	   	  
	   	 
	    
	     }
	   	              

	   	        
	    catch(Exception e){

	     System.out.println(e);

	   	        } 
	   	
	   	
	   	finally{        
	   	        
	   		
	   		
	   		//System.out.println(session.isConnected());

	   		    if (channel != null) {
	   		      session = channel.getSession();
	   		        channel.disconnect();
	   		        session.disconnect();
	   		       // System.out.println(channel.isConnected());
	   		    }
	   		
	   	      return res;
	   	}
	   	
}
	   	        
	        public static Boolean getTrayFillRDS(String min,String loc,String tray,Integer r,String DriverSheetPath,String SheetName) throws JSchException{
	        	
		   		 Channel channel=null;
		   		 Session session=null;
		   	     
		   	     boolean res=false;    

		   	        try{      
		   	              
		   	              JSch jsch = new JSch();
		   	              
		   	              java.util.Properties config = new java.util.Properties(); 
		   	              

		   	              String command = "psql --host=space-nonprod-space-rds-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_space_v1_db_sit --user tcs_testing;";
		   	       
		   	              
		   	              
		   	            session = jsch.getSession("mwadmin","10.244.39.83",22);
		   	              
		   	             config.put("StrictHostKeyChecking", "no");
		   	              
		   	              session.setConfig(config);
		   	              
		   	             
		   	              
		   	          session.setPassword("2Bchanged");
		   	          session.connect();
		                 
		                // System.out.println("Connection success");

		                 channel = session.openChannel("exec");
		                 ((ChannelExec)channel).setCommand(command);

		                 channel.setInputStream(null);
		                 ((ChannelExec)channel).setErrStream(System.err);
		                 InputStream in = channel.getInputStream();
		               ((ChannelExec)channel).setPty(false);
		                 
		                 OutputStream out=channel.getOutputStream();
		                 channel.connect();
		                 
		                 Thread.sleep(1000);
		   	              
		   	        out.write(("ed1f64006038"+"\n").getBytes());
		   	        out.flush();
		   	        
		   	        Thread.sleep(1000);

		   	    // out.write(("Select plan_name, min, store_number, space_type,total_fill_quantity ,facings,units_high,units_deep,fill_quantity,shelf,position  from space where store_number = "+loc+" and min ='"+min+"';"+"\n").getBytes());

		   	     out.write(("Select merchandise_type from space where store_number = "+loc+" and min ='"+min+"';"+"\n").getBytes());
		   	        out.flush();
		   	        
		   	        Thread.sleep(1000);
		   	        
		   	       
		   	        
		   	        byte[] tmp = new byte[10240];
		   	           
		   	        int i=0;  
		   	              
		   	        
		   	          while (in.available() > 0) {
		   	                	
		   	                	{
		   	                  
		   	                i = in.read(tmp, 0, 10240);
		   	                   
		   	                    if (i < 0) {
		   	                        break;
		   	 
		   	                    }
		   	                  
		   	                }  
		   	                	
		   	                	
		   	               
		   	               }
		   	          
		   	        String QResult=new String(tmp, 0, i); 
		   	        
		   	      //  System.out.println(QResult);
		   	           
		   	        
		   	        String [] line=   QResult.split("\n");
		   	        
		   	      	
			   	   for(int j=0;j<line.length;j++)
			   	   {
			   		   if (line[j].replaceAll("\\|", "").trim().equalsIgnoreCase("(0 rows)")) 
			   		   {
			   			   res=true;
			   			   
			   			   return res;
			   		   }
			   	   }
		   	        
		   	ArrayList<String> words = new ArrayList<String>();
		   	        
		   	
		   	
		   	   for(int j=2;j<line.length-1;j++)
		   	   {
		   		words.add(line[j].replaceAll("\\|", "").trim());
		   		   

		   	   }
		   	// System.out.println("tray: "+words);
		   	 
		   	 
		   	 if (!(words.isEmpty())) 
		   	 {
			
		   		 res=true;
			}
		   	 

		   //tray--------
		   	 	   
		   	 	   
		   	 	   for (int j = 0; j < words.size(); j++) 
		   	 	   {
		   	 		   if (!(tray.isEmpty()))
		   	 		   {
		   	 			   if (words.get(j).trim().equalsIgnoreCase("S")) 
		   	 			   {
		   	 				  tray=tray+","+"No";
		   	 			   }
		   	 			   
		   	 			   else if (words.get(j).trim().equalsIgnoreCase("T"))   
						   {
		   	 				  tray=tray+","+"Yes";
		   	 			   }
		   	 			 
		   	 		   
		   	 		   }
		   	 		   else
		   	 		   {
		   	 			  if (words.get(j).trim().equalsIgnoreCase("S")) 
		   	 			   {
		   	 				  tray="No";
		   	 			   }
		   	 			   
		   	 			   else if (words.get(j).trim().equalsIgnoreCase("T"))  
						  {
		   	 				  tray="Yes";
		   	 			   }
		   	 			 
		   	 		   }
		   	 		   
		   	 	   }
		   	 	   
		   		System.out.println("Tray Fill Value: "+tray);   	 
		   	 
		   	 excelCellValueWrite.writeValueToCell(tray, r, 19, DriverSheetPath,SheetName);
		   	   

		   	  channel.disconnect();

		   	  session.disconnect();
		   	  
		   	  Thread.sleep(5000);
		   	  
		   	  out.close();
		   	  
		   	  in.close();
		   	  
		   	 
		    
		     }
		   	              

		   	        
		    catch(Exception e){

		     System.out.println(e);

		   	        } 
		   	
		   	
		   	finally{        
		   	        
		   		
		   		
		   		//System.out.println(session.isConnected());

		   		    if (channel != null) {
		   		      session = channel.getSession();
		   		        channel.disconnect();
		   		        session.disconnect();
		   		       // System.out.println(channel.isConnected());
		   		    }
		   		
		   	      return res;
		   	}
		   	
 }


	        

 //-------------------Delivery-----------------
 
 

	public static Boolean getLastDelValueRDS(String min,String loc,String DelDate,String orderQty,String delQty,Integer r,String DriverSheetPath ,String SheetName) throws JSchException, BiffException, IOException{
	
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;
	     
	     Workbook wrk1 =  Workbook.getWorkbook(new File(DriverSheetPath));
	       
	     //Obtain the reference to the first sheet in the workbook
	  Sheet sheet1 = wrk1.getSheet(SheetName);
	  
	   DelDate=sheet1.getCell(8, r).getContents().trim();
	   
	   
	   if (DelDate.equalsIgnoreCase("N/A")) 
	   {
		   orderQty="0";
		   delQty="0";
		   res=true;
		   
		   System.out.println("Last Ordered Quantity: "+orderQty);
		   System.out.println("Last Delivered Quantity: "+delQty);
		   
		   excelCellValueWrite.writeValueToCell(orderQty, r, 10, DriverSheetPath,SheetName);
		   
		   excelCellValueWrite.writeValueToCell(delQty, r, 11, DriverSheetPath,SheetName);
		   
		   return res;
	   }
	   

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=retail-stockorder-np-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stockorder_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
           
          // System.out.println("Connection success");

           channel = session.openChannel("exec");
           ((ChannelExec)channel).setCommand(command);

           channel.setInputStream(null);
           ((ChannelExec)channel).setErrStream(System.err);
           InputStream in = channel.getInputStream();
         ((ChannelExec)channel).setPty(false);
           
           OutputStream out=channel.getOutputStream();
           channel.connect();
           
           Thread.sleep(1000);
	              
	        out.write(("daf9312c1bkl"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select quantity_ordered,quantity_receipted,date_delivery_expected from orders where ship_to_location_id='"+loc+"' and item_id='"+min+"' and date_delivery_expected='"+DelDate+"';"+"\n").getBytes());

	     //out.write(("Select * from orders where ship_to_location_id = "+loc+" and item_id ='"+min+"' limit 3;"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split(" ");
	        
	        String [] line1=   QResult.split("\n");
	        
	       // System.out.println("No of lines "+line.length);
	        
	     
	        
	        for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   res=true;
		   			   
		   			   return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
    
	   for(int j=0;j<line.length;j++)
	   {
		   if (RDS_DB.isInteger(line[j].trim())) 
		   {

			   num.add(line[j].trim());
		  	
		   }	
	   }
	   
	   //System.out.println("num: "+num);
	   
	   
	   if(!(num.isEmpty())) 
	   {
		   
		res=true;
		
	   }
	   
	   
	   orderQty=num.get(0);
	   
	   if (num.size()>1) 
	   {
		   delQty=num.get(1);
	   }
	   else
	   {
		   delQty="0";
	   }
	
	   
	   

	   	  final String OLD_FORMAT = "yyyy-MM-dd";

		  final String NEW_FORMAT = "dd/MM/yyyy";

		  // August 12, 2010
		  String oldDateString = DelDate;
		  String newDateString=null;

		  SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		  Date d = sdf.parse(oldDateString);
		  sdf.applyPattern(NEW_FORMAT);
		  newDateString = sdf.format(d);
		  
		  //System.out.println(newDateString);
	   
	   
	   
	   
	   
		  
	   System.out.println("Last Ordered Quantity: "+orderQty);
	   System.out.println("Last Delivered Quantity: "+delQty);
	   
	   excelCellValueWrite.writeValueToCell(newDateString, r, 8, DriverSheetPath,SheetName);
	   
	   excelCellValueWrite.writeValueToCell(orderQty, r, 10, DriverSheetPath,SheetName);
	   
	   excelCellValueWrite.writeValueToCell(delQty, r, 11, DriverSheetPath,SheetName);
	   
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	
	

	public static Boolean getNextDelValueRDS(String min,String loc,String DelDate,String orderQty,Integer r,String DriverSheetPath ,String SheetName) throws JSchException, BiffException, IOException{
	
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;
	     
	     Workbook wrk1 =  Workbook.getWorkbook(new File(DriverSheetPath));
	       
	     //Obtain the reference to the first sheet in the workbook
	  Sheet sheet1 = wrk1.getSheet(SheetName);
	  
	   DelDate=sheet1.getCell(9, r).getContents().trim();
	   
	   if (DelDate.equalsIgnoreCase("N/A")) 
	   {
		   orderQty="0";
   		   res=true;
		   
		   System.out.println("Next Ordered Quantity: "+orderQty);

		   excelCellValueWrite.writeValueToCell(orderQty, r, 12, DriverSheetPath,SheetName);
		   
		   return res;
	   }

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=retail-stockorder-np-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stockorder_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
           
          // System.out.println("Connection success");

           channel = session.openChannel("exec");
           ((ChannelExec)channel).setCommand(command);

           channel.setInputStream(null);
           ((ChannelExec)channel).setErrStream(System.err);
           InputStream in = channel.getInputStream();
         ((ChannelExec)channel).setPty(false);
           
           OutputStream out=channel.getOutputStream();
           channel.connect();
           
           Thread.sleep(1000);
	              
	        out.write(("daf9312c1bkl"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select quantity_ordered,date_delivery_expected from orders where ship_to_location_id='"+loc+"' and item_id='"+min+"' and date_delivery_expected='"+DelDate+"';"+"\n").getBytes());

	     //out.write(("Select * from orders where ship_to_location_id = "+loc+" and item_id ='"+min+"' limit 3;"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split(" ");
	        
	        String [] line1=   QResult.split("\n");
	        
	       // System.out.println("No of lines "+line.length);
	       
	      
	        for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   res=true;
		   			   
		   			   return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
    
	   for(int j=0;j<line.length;j++)
	   {
		   if (RDS_DB.isInteger(line[j].trim())) 
		   {

			   num.add(line[j].trim());
		  	
		   }	
	   }
	   
	   //System.out.println("num: "+num);
	   
	   
	   if(!(num.isEmpty())) 
	   {
		   
		res=true;
		
	   }
	   
	   
	   orderQty=num.get(0);
	   
	   
	   
	   

	   	  final String OLD_FORMAT = "yyyy-MM-dd";

		  final String NEW_FORMAT = "dd/MM/yyyy";

		  // August 12, 2010
		  String oldDateString = DelDate;
		  String newDateString=null;

		  SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		  Date d = sdf.parse(oldDateString);
		  sdf.applyPattern(NEW_FORMAT);
		  newDateString = sdf.format(d);
		  
		  //System.out.println(newDateString);
	   
	   
	
	

	   System.out.println("Next Ordered Quantity: "+orderQty);
	   
	   excelCellValueWrite.writeValueToCell(newDateString, r, 9, DriverSheetPath,SheetName);
	   
	   excelCellValueWrite.writeValueToCell(orderQty, r, 12, DriverSheetPath,SheetName);
	   
	  
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	

	public static Boolean getDelDatesRDS(String min,String loc,String lastDelDate,String nextDelDate,String currentDate,Integer r,String DriverSheetPath ,String SheetName) throws JSchException{
	
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=retail-stockorder-np-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stockorder_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
           
          // System.out.println("Connection success");

           channel = session.openChannel("exec");
           ((ChannelExec)channel).setCommand(command);

           channel.setInputStream(null);
           ((ChannelExec)channel).setErrStream(System.err);
           InputStream in = channel.getInputStream();
         ((ChannelExec)channel).setPty(false);
           
           OutputStream out=channel.getOutputStream();
           channel.connect();
           
           Thread.sleep(1000);
	              
	        out.write(("daf9312c1bkl"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select date_delivery_expected from orders where ship_to_location_id='"+loc+"' and item_id='"+min+"' order by date_delivery_expected desc limit 2;"+"\n").getBytes());

	     //out.write(("Select * from orders where ship_to_location_id = "+loc+" and item_id ='"+min+"' limit 3;"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	       // System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split(" ");
	        
	        String [] line1=   QResult.split("\n");
	        
	     /*   System.out.println("No of lines "+line.length);
	        
	      for (int j = 0; j < line1.length; j++) 
	      {
			System.out.println("Text :"+line1[j]);
	      }*/
	        
	        for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   
		   			 lastDelDate="N/A";
		   			 nextDelDate="N/A";
		   			 res=true;
		   			   
		   		   System.out.println("Last Delivery Date: "+lastDelDate);
		   		   System.out.println("Next Delivery Date: "+nextDelDate);
		   		   
		   		  
		   		   excelCellValueWrite.writeValueToCell(lastDelDate, r, 8, DriverSheetPath,SheetName);
		   		   excelCellValueWrite.writeValueToCell(nextDelDate, r, 9, DriverSheetPath,SheetName);
		   		   
		   			 return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
    
	   for(int j=2;j<line1.length-1;j++){
	
	       
	    	num.add(line1[j].trim());
	  	 
	   }
	   
	  // System.out.println("num: "+num);
	   
	   
	   if(!(num.isEmpty())) 
	   {
		   
		res=true;
		
	   }
	   
	   
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       Date date1 = sdf.parse(num.get(0));
       Date date2 = sdf.parse(currentDate);

      //System.out.println("date1 : " + sdf.format(date1));
      //System.out.println("date2 : " + sdf.format(date2));

       if (date1.after(date2)) 
       {
    	   nextDelDate=num.get(0);
    	   lastDelDate=num.get(1);
       }

       if (date1.before(date2)) 
       {
    	   lastDelDate=num.get(0);
    	   nextDelDate="N/A";
       }

       if (date1.equals(date2)) 
       {
    	   nextDelDate=num.get(0);
    	   lastDelDate=num.get(1);
       }

	  

	   System.out.println("Last Delivery Date: "+lastDelDate);
	   System.out.println("Next Delivery Date: "+nextDelDate);
	   
	  
	   excelCellValueWrite.writeValueToCell(lastDelDate, r, 8, DriverSheetPath,SheetName);
	   excelCellValueWrite.writeValueToCell(nextDelDate, r, 9, DriverSheetPath,SheetName);
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	
 
	
//--------------StockRecord--------------------
	
	
	public static Boolean getStockRecordRDS(String min,String loc,String stockRecord,String currentDate,Integer r,String DriverSheetPath ,String SheetName) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
          
         // System.out.println("Connection success");

          channel = session.openChannel("exec");
          ((ChannelExec)channel).setCommand(command);

          channel.setInputStream(null);
          ((ChannelExec)channel).setErrStream(System.err);
          InputStream in = channel.getInputStream();
        ((ChannelExec)channel).setPty(false);
          
          OutputStream out=channel.getOutputStream();
          channel.connect();
          
          Thread.sleep(1000);
	              
	        out.write(("f55c9c05877d"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select item_id, last_updated_date ,qty,location_id from stock where location_id='"+loc+"' and item_id='"+min+"';"+"\n").getBytes());


	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    if (i < 0) 
	                    {
	                        break;
	 
	                    }
	                  }  
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split("\\|");
	        
	        String [] line1=   QResult.split("\n");
	        
	      System.out.println("No of lines "+line.length);
	   /*     
	      for (int j = 0; j < line.length; j++) 
	      {
			System.out.println("Text :"+line[j].trim());
	      }
	      
	      */  for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   stockRecord="Not available";
		   			   
		   			   res=true;
		   			   
		   			 System.out.println("Stock Record: "+stockRecord);
		   		   
		   			 excelCellValueWrite.writeValueToCell(stockRecord, r, 8, DriverSheetPath,SheetName);
		   		   
		   			   return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
   
	   for(int j=2;j<line.length;j++){
	
	       if (RDS_DB.isInteger(line[j].trim())) 
	       {
		
	    	num.add(line[j].trim());
	       } 
	   }
	   
	  // System.out.println("num: "+num);
	   
	   
	  
	  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      Date date1 = sdf.parse(line[4].trim());
      Date date2 = sdf.parse(currentDate);

   /*  System.out.println("date1 : " + sdf.format(date1));
     System.out.println("date2 : " + sdf.format(date2));
*/
      if (date1.equals(date2)) 
      {
    	  		stockRecord=num.get(0);
    	  		
    	  		res=true;
			   
      }
      
      else
      {
    	  	stockRecord="Updating";
		     
    	  	res=true;
      }

      System.out.println("Stock Record: "+stockRecord);
	    
	  
	   excelCellValueWrite.writeValueToCell(stockRecord, r, 8, DriverSheetPath,SheetName);
	  
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	
	public static Boolean getStockHistoryRDS(String min,String loc,String user ,String qty,String reasonCode,Integer r,String DriverSheetPath ,String SheetName) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
         
        // System.out.println("Connection success");

         channel = session.openChannel("exec");
         ((ChannelExec)channel).setCommand(command);

         channel.setInputStream(null);
         ((ChannelExec)channel).setErrStream(System.err);
         InputStream in = channel.getInputStream();
       ((ChannelExec)channel).setPty(false);
         
         OutputStream out=channel.getOutputStream();
         channel.connect();
         
         Thread.sleep(1000);
	              
	        out.write(("f55c9c05877d"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select created,user_id,value,reason_code,location_id from transaction_hist where location_id='"+loc+"' and min='"+min+"' order by created desc limit 5;"+"\n").getBytes());


	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    if (i < 0) 
	                    {
	                        break;
	 
	                    }
	                  }  
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split("\\|");
	        
	        String [] line1=   QResult.split("\n");
	        
	        /*   System.out.println("No of lines "+line.length);
	       
	      for (int j = 0; j < line.length; j++) 
	      {
			System.out.println("Text :"+line[j].trim());
	      }
	      
	      */  for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   res=true;
		   			   
		   			   return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
  
	   for(int j=0;j<line.length;j++){
	
	       if (RDS_DB.isInteger(line[j].trim())) 
	       {
		
	    	num.add(line[j].trim());
	       } 
	   }
	   
	   //System.out.println("num: "+num);
	   
	   if (!num.isEmpty()) 
	   {
		res=true;
	   }
	   
	
	   
	   
//user--------------	   
	   
	   for (int j = 0; j < (num.size())/3; j++) 
	   {
		   if (!(user.isEmpty()))
		   {
		
			   user=user+","+num.get((j*3)+0).trim();
		   
		   }
		   else
		   {
			   user=num.get((j*3)+0).trim();
			   
		   }
		   
	   }
	   
	   
//Quantity--------------	   
	   
	   for (int j = 0; j < (num.size())/3; j++) 
	   {
		   if (!(qty.isEmpty()))
		   {
		
			   qty=qty+","+num.get((j*3)+1).trim();
		   
		   }
		   else
		   {
			   qty=num.get((j*3)+1).trim();
			   
		   }
		   
	   }
	   
	   
	   
//ReasonCode--------------	   
	   
	   for (int j = 0; j < (num.size())/3; j++) 
	   {
		   if (!(reasonCode.isEmpty()))
		   {
		
			   reasonCode=reasonCode+","+num.get((j*3)+2).trim();
		   
		   }
		   else
		   {
			   reasonCode=num.get((j*3)+2).trim();
			   
		   }
		   
	   }
	   
	   
	   
	   
	   
	   System.out.println("Users: "+user);
	   
	   System.out.println("Quantity: "+qty);
	   
	   System.out.println("Reason Code: "+reasonCode);
	    
	  
	   excelCellValueWrite.writeValueToCell(user, r, 12, DriverSheetPath,SheetName);
	   excelCellValueWrite.writeValueToCell(qty, r, 13, DriverSheetPath,SheetName);
	   excelCellValueWrite.writeValueToCell(reasonCode, r, 11, DriverSheetPath,SheetName);
	   
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	
	

	public static Boolean getStockHistoryDateTimeRDS(String min,String loc,String dates ,String time,Integer r,String DriverSheetPath ,String SheetName) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false;

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
         
        // System.out.println("Connection success");

         channel = session.openChannel("exec");
         ((ChannelExec)channel).setCommand(command);

         channel.setInputStream(null);
         ((ChannelExec)channel).setErrStream(System.err);
         InputStream in = channel.getInputStream();
       ((ChannelExec)channel).setPty(false);
         
         OutputStream out=channel.getOutputStream();
         channel.connect();
         
         Thread.sleep(1000);
	              
	        out.write(("f55c9c05877d"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select created from transaction_hist where location_id='"+loc+"' and min='"+min+"' order by created desc limit 5;"+"\n").getBytes());


	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    if (i < 0) 
	                    {
	                        break;
	 
	                    }
	                  }  
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        //System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split("\\|");
	        
	        String [] line1=   QResult.split("\n");
	    /*    
	     // System.out.println("No of lines "+line1.length);
	        
	      for (int j = 0; j < line1.length; j++) 
	      {
			System.out.println("Text :"+line1[j].trim());
	      }
	      */
	        for(int j=0;j<line1.length;j++)
		   	   {
	        	
		   		   if (line1[j].trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			   res=true;
		   			   
		   			   return res;
		   		   }
		   	   }

	   ArrayList<String> num = new ArrayList<String>();
  
	   for(int j=2;j<7;j++)
	   {
	
		   num.add(line1[j].trim());
	       
	   }
	   
	   //System.out.println("num: "+num);
	   
	   if (!num.isEmpty()) 
	   {
		res=true;
	   }
	   
	 
	   
//----------Date-------------
	   
	  
	 
  ArrayList<String> rds_dd = new ArrayList<String>();
	   
	   
	   for (int j = 2; j < 5; j++) 
	   {
		
		   rds_dd.add(num.get(j).substring(8,10).trim());
		   
	   }
	   
	   
	   
  ArrayList<String> rds_mm = new ArrayList<String>();
	   
	   
	   for (int j = 2; j < 5; j++) 
	   {
		
		   rds_mm.add(num.get(j).substring(5,7).trim());
		   
	   }
	
	   
   ArrayList<String> rds_dates = new ArrayList<String>();
	   
   rds_dates.add("Today");
   
   rds_dates.add("Yesterday");
   
   for (int j = 0; j < 3; j++) 
   {
	
	   rds_dates.add(rds_dd.get(j)+"/"+rds_mm.get(j));
	   
   }
	 
	      
	   
	   
//----------Time-------------
	   
   
   ArrayList<String> rds_hh = new ArrayList<String>();
   
   
   for (int j = 0; j < 5; j++) 
   {
	
	   rds_hh.add(num.get(j).substring(11,13).trim());
	   
   }
   
   
   
ArrayList<String> rds_min = new ArrayList<String>();
   
   
   for (int j = 0; j < 5; j++) 
   {
	
	   rds_min.add(num.get(j).substring(14,16).trim());
	   
   }

	   
   ArrayList<String> rds_time = new ArrayList<String>();
	   
	   
	   for (int j = 0; j < 5; j++) 
	   {
		
		   if (Integer.parseInt(rds_hh.get(j))<11)
		   {
			   rds_time.add((Integer.parseInt(rds_hh.get(j))+1)%24+":"+rds_min.get(j)+" AM");
		   }
		   
		   else
		   {
			   rds_time.add((Integer.parseInt(rds_hh.get(j))+1)%24+":"+rds_min.get(j)+" PM");
		   }
	   }
	  
	   
	   
	   
	   
	   
//dates--------------	   
	   
	   for (int j = 0; j < rds_dates.size(); j++) 
	   {
		   if (!(dates.isEmpty()))
		   {
		
			   dates=dates+","+rds_dates.get(j).trim();
		   
		   }
		   else
		   {
			   dates=rds_dates.get(j).trim();
			   
		   }
		   
	   }
	   
//time--------------	   
	   
	   for (int j = 0; j < rds_time.size(); j++) 
	   {
		   if (!(time.isEmpty()))
		   {
		
			   time=time+","+rds_time.get(j).trim();
		   
		   }
		   else
		   {
			   time=rds_time.get(j).trim();
			   
		   }
		   
	   }
	      
	   
	   
	   
	   
	   
	   System.out.println("Date: "+dates);
	   System.out.println("Time: "+time);
	  
	   excelCellValueWrite.writeValueToCell(dates, r, 9, DriverSheetPath,SheetName);
	   excelCellValueWrite.writeValueToCell(time, r, 10, DriverSheetPath,SheetName);
	   
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
	        
}	        
	
	

  public static void main(String[] args) throws Exception {

    	 String min=null;  
    	 // 107842831
    	 //100100594
  	     String loc="89";
  	     
  	   
  	/*     
  	   min = Space_RDS_DB.getItemMinRDS(min, loc);
  	   
  	   System.out.println("Min :"+min);
  	   
  	   Space_RDS_DB.getSpaceValueRDS(min, loc);
    
  	   Space_RDS_DB.getPlanNamesRDS(min, loc);
    	 
   */

     }

	
  
  public static Boolean get_spaceMin(String[] min,String query,Integer r,String DriverSheetPath,String SheetName) throws JSchException{
  	
		 Channel channel=null;
		 Session session=null;
	     
	     boolean res=false; 
	     
	     String item="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              

	              String command = "psql --host=space-nonprod-space-rds-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_space_v1_db_sit --user tcs_testing;";
	       
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword("2Bchanged");
	          session.connect();
           
          // System.out.println("Connection success");

           channel = session.openChannel("exec");
           ((ChannelExec)channel).setCommand(command);

           channel.setInputStream(null);
           ((ChannelExec)channel).setErrStream(System.err);
           InputStream in = channel.getInputStream();
         ((ChannelExec)channel).setPty(false);
           
           OutputStream out=channel.getOutputStream();
           channel.connect();
           
           Thread.sleep(1000);
	              
	        out.write(("ed1f64006038"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	    // out.write(("Select plan_name, min, store_number, space_type,total_fill_quantity ,facings,units_high,units_deep,fill_quantity,shelf,position  from space where store_number = "+loc+" and min ='"+min+"';"+"\n").getBytes());

	     out.write((query+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	       
	        
	        byte[] tmp = new byte[10240];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 10240);
	                   
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	                }  
	                	
	                	
	               
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	        
	        System.out.println(QResult);
	           
	        
	        String [] line=   QResult.split("\n");
	        
	   
	     for(int j=0;j<line.length;j++)
 	   {
 		   if (line[j].replaceAll("\\|", "").trim().equalsIgnoreCase("(0 rows)")) 
 		   {
 			   res=false;
 			   
 			   return res;
 		   }
 	   }      
	        
	        String [] line2=   QResult.split("min");
	        
	        String [] line1=   line2[1].split("\n");

	 	   
	 		   
	 		   item = line1[2].replaceAll("\\|", "").trim();

	 		   min[0] =item;
	 		   res=true;
		        
	 		   System.out.println(item);

	  
	// System.out.println("Plans: "+words);
	 
	 
	 excelCellValueWrite.writeValueToCell(item, r, 6, DriverSheetPath,SheetName);
	   

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}
	
}
	        

	
}
