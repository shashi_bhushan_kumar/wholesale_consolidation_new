package McColls_I5a_All_Functions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;




import org.apache.poi.xwpf.usermodel.XWPFRun;

import Utilities_i5A_All.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class McColls_I5a_GetCall {
	
	public static boolean GetCallItemStatusValidation_Invalid(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		
		System.out.println(statusCurrentValue);
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];
		
		String ShipOrderStatus=shipStatusArrey[1];
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 9) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 9");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
					
				break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	public static String GetCallOrderOpsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreId,String refCode, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {
		
		String res = null;
		
		String lookupsJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	       
	        
	       // HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/stores/items/" + StoreId + "?" + ApiKey+"&filtertype=maptype&filtervalue=CLIENTID");
	      
	        
	        HttpGet httpget = new HttpGet("/wholesale/v1/customers/mccolls/stores/" + StoreId + "?" + ApiKey);
	        
	      //  https://sit-api.morrisons.com/wholesale/v1/customers/mccolls/stores/5672?apikey=LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz
	        
	        
	        
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Store ID is " + StoreId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "StoreId "+StoreId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	       
	        
	        
	        
	        System.out.println("Output json is "+jsonresponse);
	        
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());

	        utilityFileWriteOP.writeToLog(tcid, "Order OPS -> Webportal Response Body is ",jsonobject.toString(),Resultpath,xwpfRun,"");
	        

	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
			    
			    
			    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
			    	
			    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
		        	
		        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
			        
		        	System.out.println(lookupsJsonKey);
		        	System.out.println("ORDER-OPS-"+refCode.trim());
		        	
		        	if(lookupsJsonKey.equals("ORDER-OPS-"+refCode.trim())) {
		        		
		        		
		        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
			        		
					        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

					        lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}
		        	
	 	        		
		        	}
		    	
			    }
		   
			   	     
			    res = lookupsJsonvalue;
			    
			    
			    utilityFileWriteOP.writeToLog(tcid, "Order OPS value  in Webportal for  the store is  "+StoreId, res);
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}

	
	
	
public static String GetCallOrderOpsFromWebPortalV1(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreId,String refCode, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {

	
	String res = null;
		
	String lookupsJsonvalue = null;
		
	CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();

	        HttpGet httpget = new HttpGet("/wholesale/v1/customers/sandpiper/stores/" + StoreId + "?" + ApiKey);

	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        response = httpclient.execute(target, httpget);
	        

	        String jsonresponse = EntityUtils.toString(response.getEntity());

	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        
	        JsonObject jsonobject = jsonelement.getAsJsonObject();

	        JsonArray jarray = jsonobject.getAsJsonArray("stores");
	        
	        
	        jsonobject = jarray.get(0).getAsJsonObject();

	        JsonArray jarray1 = jsonobject.getAsJsonArray("categories");

	        for(int i=0; i<jarray1.size(); i++){
	        	
	        	
	        JsonObject jsonobjectlookupsBody = jarray1.get(i).getAsJsonObject();
	        	
	        String ORCode=  jsonobjectlookupsBody.get("name").toString().replaceAll("^\"|\"$", "");

	        	 
	        if(ORCode.contentEquals(refCode))	{
	        
	        
	            
	            JsonArray deliveryOp=     jsonobjectlookupsBody.getAsJsonArray("deliveryOpportunities");

	            JsonObject jsonobjectlookupsBody1 = deliveryOp.get(0).getAsJsonObject();
	            
	            
	          String OpsValue=  jsonobjectlookupsBody1.get("value").toString().replaceAll("^\"|\"$", "");
	            
	           System.out.println("OPS Value is "+OpsValue);
	            
	            res=OpsValue;
	            
 	
	        }
	            
	        }

		 
		 
	     utilityFileWriteOP.writeToLog(tcid, "Order OPS value in Webportal for  the store is  "+StoreId, res);
				
	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			
			
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}

	
	
	
	
	
	
	
	
	
	
	
	public static String GetCallStoreAddressFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String StoreId, String tcid, String Resultpath,XWPFRun xwpfRun) throws IOException {
		
		String res = null;
		
		String storeAddress = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/stores/items/" + StoreId + "?" + ApiKey+"&filtertype=maptype&filtervalue=CLIENTID");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Store ID is " + StoreId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "StoreId "+StoreId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ",jsonresponse,Resultpath,xwpfRun,"");
	        
	        
	        
	        
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
		        
		        storeAddress = description.split(":")[1].trim(); 
	        	
			   
			   	     
			    res = storeAddress;
			    
			    
			    
			    
			    utilityFileWriteOP.writeToLog(tcid, "StoreName in WebPortal "+res,"",Resultpath,xwpfRun,"");
		        
		        
			    
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
	public static String GetCallWSCPorWSPFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String quantityType, String tcid, String Resultpath) throws IOException {
		
		String res = null;
		
		String pricesJsonvalue = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("ItemID ID is " + ItemID);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
			    
			    
			    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
			    	
			    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
		        	
		        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
			        
		        	if(quantityType.equals("CS")) {
		        		
		        		if(pricesJsonKey.equals("WSCP")) {
		        			
		        			
		        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
				        		
						        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

						        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
		        	}
		        	
		        	if(quantityType.equals("EA")) {
		        		
		        		if(pricesJsonKey.equals("WSP")) {
		        			
		        			
		        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
				        		
						        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

						        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
						     
				        	}
		        			
		        			
		        		}
		        		
		        	}
		        	
		        	/*if(pricesJsonKey.equals("ORDER-OPS")) {
		        		
		        		
		        		for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}
		        	
	 	        		
		        	}*/
		    	
			    }
		   
			   	     
			    res = pricesJsonvalue;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	public static String GetCallquantityTypeFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
		
		String res = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
	        
	        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Item ID is " + ItemId);
	        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
	        
	     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularWaitingTime);*/
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		
		        
			    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
			    
			    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
			    ArrayList<String> listoflookupkeys = new ArrayList<String>();
			    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

			    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

			    
			    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
			    	
			    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
		        	
		        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
		        	
		        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
			        
		        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
		        		
		        		
		        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
			        		
					        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

					        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
					       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
					        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
					    	
					    	long startdatemilli = date.getTime();
					        
					        mapofquantityType.put(lookupsJsonvalue+String.valueOf(lookupsBodyindex), startdatemilli);
			        		
			        	}
		        	
	 	        		
		        	}
		    	
			    }
			    System.out.println("mapofquantityType.size() "+mapofquantityType.size());
			    for(String str: mapofquantityType.keySet()) {
			    	
			    	listoflookupkeys.add(str);
			    	listoflookupstartvalues.add(mapofquantityType.get(str));
			    }

			    long latestdatemilli = 0; 
			    
			    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
			    			    	
			    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
			    		
			    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
			    		
			    	}
			    	
			    }
			    
			    String finalSinglePickkey = null;
			    String quantityTypeFromWebPortal = null;
			    
			    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
			    	
			    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
			    		
			    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
			    	}
			    	
			    }
			    
			    System.out.println("latest date in milli "+latestdatemilli);
			    
			    if(finalSinglePickkey.contains("Y")) {
			    	
			    	quantityTypeFromWebPortal = "EA";
			    }
			    
			    if(finalSinglePickkey.contains("N")) {
			    	
			    	quantityTypeFromWebPortal = "CS";
			    }
			    
			    
			    //String itemDetailsbody = "{\"quantityType\": \"" + quantityTypeFromWebPortal + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
			    	    
			    //System.out.println(itemDetailsbody);	    
			    	     
			    res = quantityTypeFromWebPortal;
				
				
	        	
	        }
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
	        
			res = null;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
		
		boolean res = false;
		
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];

		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        
	        
	        for (int j=0;j<120;j++){
	        
	        
	       Thread.sleep(5000);
	        	
	        	
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);

	        
	        
	      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        
	        
	      //  System.out.println(jsonresponse);
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
	       
	        
	     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
	      //  System.out.println(jarray.size());
	     //   for(int i=0; i<jarray.size(); i++) {
	        
	      //  System.out.println(jsonobject.toString());
	        
	        
	        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        
	    //  System.out.println(jarray);
	       
	    
		        
	        	
	        //String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        //System.out.println("StatusValidation Result is " + statusValidationResult);

		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfrun,"");
					
					
					System.out.println("Orders statusCurrent is " +statusCurrent);
					
				     break;
				
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfrun,"");
					System.out.println("statusCurrent is " +statusCurrent);
					
					continue;
		        	
		        }
	        	
	       
	        }
	        
	      
	    	
		}  //end of try
	    
	    
	    
	    catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
			//return res;
		}
	    
	    finally{
	        
	     response.close();
	     
	     return res;
	        
	}
	    
	    
	    
		
	}
	
	
	
	
	
	
public static String  GetOrderOPS(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	String OPS=null;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
 
       Thread.sleep(10000);
        	
        	
      //  System.out.println("Order ID is " + OrderID);
      //  utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
      //  System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
       String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
        
        OPS = jsonobject1.get("orderOPS").toString().replaceAll("^\"|\"$", "");
        
        
        
        utilityFileWriteOP.writeToLog(tcid, "Order OPS value  for order "+OrderID+"is ", OPS);
        
        
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	       

       
      
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return OPS;
        
}
    
    
    
	
}


public static String  GetOrderStoreName(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
	
boolean res = false;
	
	String OPS=null;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
 
       Thread.sleep(10000);
        	
        	
      //  System.out.println("Order ID is " + OrderID);
      //  utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
      //  System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
       String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
      //  utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
        
        OPS = jsonobject1.get("storeName").toString().replaceAll("^\"|\"$", "");
        
        
        
        utilityFileWriteOP.writeToLog(tcid, "Order StoreName value  for order "+OrderID+"is ", OPS);
        
        utilityFileWriteOP.writeToLog(tcid, "StoreName in OrderDetails is  "+OPS,"",ResultPath,xwpfrun,"");
         
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	       

       
      
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return OPS;
        
}
    
    
    
    
	
}

	
	
	
	
public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        
	        int stCnt=jarray.size();
	        
	        int mtchCnt=0;
	        
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        
		        
		        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
		        
		        
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = true;
		        	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 10");
		       
		        
		        }
		        else {
		        	
		        	res = false;
					
		        	
		        	utilityFileWriteOP.writeToLog(tcid, "Item StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	//break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("Item statusCurrent is " +statusCurrent);
					
					mtchCnt=mtchCnt+1;
					
					
				//break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					
					
					System.out.println("Item statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	       
	        if(mtchCnt==stCnt) {
	        	break;
	        
	        }
	        
	      }
	    	
} 
	    
	    
catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}
	
	



public static HashMap<String, String> GetCallItemStatusValidation_PriceConfirmedAmount(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	HashMap<String, String> PriceConFirmedAmt = new HashMap<String, String>();
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
       
        
        System.out.println(jarray.size());
 for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	        
	        
	      //  String ItemId=jsonobject.get("itemID").toString().replaceAll("^\"|\"$", "");
	        
	        String PriceConfirmedAmt = jsonobject.get("priceConfirmedAmount").toString().replaceAll("^\"|\"$", "");
	        
	        
	        
	        PriceConFirmedAmt.put(ItemIDs,PriceConfirmedAmt);

	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	        

        	
        }
  
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return PriceConFirmedAmt;
        
    }

}









public static boolean GetCallItemStatusValidation_InvalidCase_InvalidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	 boolean res = true;
	
	
	String [] AllItems=ItemIDDs.split(",");
	
	
	
	
	
	int passCount=0;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		
				 System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}



public static boolean GetCallItemStatusValidation_InvalidLocation_validItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	String [] AllItems=ItemIDDs.split(",");
	
	
	
	
	
	int passCount=0;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        int pCount=0;
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	        
	    if(i==0)  { 
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = false;
	        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
	        	
	
			    System.out.println("Count of 1 is 10");
	       
	           
	        }
	       
	        
	       else {
	        	
	        	
	    	   res=res&&true;
	    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

               }        		

         }
	    
	    
	    
	    
if(i==1)  { 
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = false;
	        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
	
			    System.out.println("Count of 1 is 10");
	       
	        
	        }
	       
	        
	       else {
	       
	    	   res=res&&true;
	    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

            }        		

 }
	 
 

   }

   
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}

















public static boolean GetCallItemStatusValidation_InvalidDateAndValidItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String ItemIDDs,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = true;
	
	
	String [] AllItems=ItemIDDs.split(",");
	
	
	
	
	
	int passCount=0;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
      //  for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(15000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        
        int pcount=0;
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	    
	       String ItemIDs= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
	        
	       utilityFileWriteOP.writeToLog(tcid, "Item ID "+ItemIDs+"StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	       
	       
	        
	       if(i==0)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        	//System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		        	
		
				    System.out.println("Count of 1 is 10");
		       
		           
		        }
		       
		        
		       else {
		        	
		        	
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);   

	               }        		

	         }
		    
		    
		    
		    
	if(i==1)  { 
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 10) {
		        	res = false;
		        //	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is not as expected ", "for Item ID:"+ItemIDs);
		
				    System.out.println("Count of 1 is 10");
		       
		        
		        }
		       
		        
		       else {
		       
		    	   res=res&&true;
		    	   utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is as expected ", "for Item ID:"+ItemIDs);

	            }        		

	 }
		 
	 

	   }

	   
	    	
	} 
	    
	    
	catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	    }

	}







public static boolean GetCallItemStatusValidation_negativeScenarios(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	      /*  if(countOne == 9) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 9");
	        }
	        else {
	        	
	        	res = false;
				
	        	
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }*/
	        System.out.println(countOne);
	        System.out.println(statusCurrent);
	        System.out.println(statusCurrentValue);
	        
	        if( (statusCurrent.equals(statusCurrentValue)) && (countOne != 9)) {
	        	res = true;
	        	
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}


	
	
	
public static boolean GetCallItemStatusValidation_InvalidItems(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
	
	boolean res1 = false;
	
	boolean res2=false;
	
	
	boolean res=false;
	
	String [] StatusValues=statusCurrentValue.split(",");
	
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
         	
        Thread.sleep(10);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
       
        
        for(int i=0; i<jarray.size(); i++) {
        	
        statusCurrentValue=StatusValues[i+1];
        
        
     
        		 jsonobject = jarray.get(i).getAsJsonObject();
        		
        		
        	//	String itemLineId= jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");

        		   String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");

        		   if(statusCurrent.equals(statusCurrentValue)) {
        		        	
        			   res1=true;
        					
        			   
        			   utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					
        					
        					
        					System.out.println("statusCurrent is " +statusCurrent);
        					
        				//continue;
        				
        				
        		        }
        		
        		        
        		        
        		       
        		        else{
        		        	
        		        	res1=false;
        		        	
        		        	
        		        	
        					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					System.out.println("statusCurrent is " +statusCurrent);

        		        }

        }

    	
	} catch (Exception e) {
		// TODO: handle exception
		res1=false;
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
       
	}
    
    finally{
        
        response.close();
        
        return res1;
        
}
    
  
    
	
}


	
	
	
	
	
public static boolean GetCallItemStatusValidationOnlyConfirmed(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne != 7) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is not 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	break;
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}


public static boolean GetOrderDetailsResponse(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid,String ResultPath, XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        
        for (int j=0;j<1;j++){
        
        
       Thread.sleep(2000);
        	
        	
        System.out.println("Order ID is " + OrderID);
       // utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
        System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
   res = true; 
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET ORDER call ", "Due to :"+e);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
    	
}


public static boolean GetCallItemDetailsResponse(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        //utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= 10000; waitingTime = waitingTime + 10000) {
        	
        	
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
      res = true;
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET ITEM call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}



}
