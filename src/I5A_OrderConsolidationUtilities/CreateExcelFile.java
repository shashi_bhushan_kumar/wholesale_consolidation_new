package I5A_OrderConsolidationUtilities;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class CreateExcelFile {

	
		
		public static boolean CreateTempExcel() {
			
			
			
			//TestData/I5aOrderItemsNAS_fileCreation.xls
			
			Boolean res=false;
			FileOutputStream fileOut =null;
			
			try {
			
				
			fileOut = new FileOutputStream("TestData/I5aOrderItemsNAS_fileCreation.xls");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("OrderItems");

			workbook.createSheet("OrderItemsNAS");

			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			
			
			
			System.out.println("Excel file created!!!");
			
			
			res=true;
		} 
		
		
		
		
		catch (Exception e) {
			e.printStackTrace();
		} 
		
			
			finally{
				
			
				try {
					fileOut.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return res;	
			
			
			}
		
		}

		

		
		
/*public static void main(String[] args) {	// TODO Auto-generated method stub

	CreateExcelFile.CreateTempExcel();
	
	
	
	
	}*/

}
