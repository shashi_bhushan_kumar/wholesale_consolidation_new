package Utilities_v1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

public class UpdateDataSheet {
	
	public static boolean update(String TestKeyword, String DriverSheetPath, int SheetNo, int rows, int col, String newvalue) throws BiffException, IOException {
		
		int r = 0;
		int occurances = 0;
		
		
		
		Workbook wrk1 = Workbook.getWorkbook(new File(DriverSheetPath));
		Sheet sheet1 = wrk1.getSheet(SheetNo);
		
		try{
			for(r=1;r<rows;r++) {
				
				String keyword = null;
				
				
				keyword = sheet1.getCell(2, r).getContents().trim();
				
				if(occurances>0) {
					  if(!TestKeyword.contentEquals(keyword)){
						  break;
					  }
				  }
				
				if(keyword.equalsIgnoreCase(TestKeyword)) {
					  occurances = occurances+1;
					  
	
					
					  FileInputStream file = new FileInputStream(DriverSheetPath);
					  
					  HSSFWorkbook workbook = new HSSFWorkbook(file);
					  HSSFSheet sheet = workbook.getSheetAt(0);
					  Cell cell = null;
					  
					  HSSFRow sheetrow = sheet.getRow(r);
					  
					  if(sheetrow==null) {
						  sheetrow = sheet.createRow(r);
					  }
					  
					  cell = sheetrow.getCell(col);
					  
					  if(cell==null) {
						  cell = sheetrow.createCell(col);
					  }
					  
					  cell.setCellValue(newvalue);
					  
					  file.close();
					  
					  FileOutputStream outfile = new FileOutputStream(DriverSheetPath);
					  workbook.write(outfile);
					  outfile.close();
					  
					  workbook.close();
					  
					 
					  
					  
				}
				
				
			}
			
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		
		
		return true;
	}

}
