package Utilities_v1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class ProjectConfigurations {
	
	
  public static String LoadProperties(String key) {

	Properties prop = new Properties();
	InputStream input = null;
	
	String Value=null;

	try {

		input = new FileInputStream("Configuration/config.properties");

		// load a properties file
		prop.load(input);
		
		
		Value=prop.getProperty(key);
		
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
return 	Value;
		
	}
  }
	
	public static Properties LoadConfigFile() {

		Properties prop = new Properties();
		InputStream input = null;
		
		String Value=null;

		try {

			input = new FileInputStream("Configuration/config.properties");

			// load a properties file
			prop.load(input);
			
			
			//Value=prop.getProperty(key);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			
	return 	prop;
			
		}

  }
}