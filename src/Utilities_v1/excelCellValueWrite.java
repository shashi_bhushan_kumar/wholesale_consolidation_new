package Utilities_v1;

import java.io.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excelCellValueWrite {
	
	
	
	
public static void writeValueToCell_sheet (String res, int sheet_no, int rowNo, int columnNo,String resultFilePath ) throws IOException
{
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
       
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
      
         HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(sheet_no); 	
         
         Cell cell = null; 
       
         
         cell = my_worksheet.getRow(rowNo).getCell(columnNo);
       
         if (cell == null)
         
        	 cell = my_worksheet.getRow(rowNo).createCell((short)columnNo);
        
         
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
       
         cell.setCellValue(res);
        
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         
         my_xls_workbook.write(output_file);
         
         output_file.close(); 
         my_xls_workbook.close();
}
	

	public static void writeValueToCell(String res,int i,int columnNo,String resultFilePath ) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0); 	
		
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();
	}
	public static void writeValueToCell(String res,int i,int columnNo,String resultFilePath, String sheetName ) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         HSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
		
         
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();      
	}
	
public static void writeValueToCellMacro(String res,int i,int columnNo,String resultFilePath, String sheetName) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		XSSFWorkbook my_xls_workbook = new XSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         XSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
		
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(XSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();   
	}

	
}
