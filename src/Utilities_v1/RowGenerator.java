package Utilities_v1;	
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class RowGenerator {
    public static void main(String[] args) throws Exception {
    	
    	//ResultLogPath=ProjectConfigurations.LoadProperties("SalesforceAutomation_TestDataPath");
    	
    }
   

    /*public static void collateHTMLLog()
    {
    	try
    	{
    		BufferedReader br = new BufferedReader(
                    new FileReader("C:/Users/SYSTCSSR/Desktop/SP2_TC01/Template.txt"));
        	
        	//BufferedReader br1 = new BufferedReader(
                    //new FileReader("C:/Users/SYSTCSSR/Desktop/SP2_TC01/EachRow.txt"));
        	BufferedReader br1 = new BufferedReader(
                    new FileReader("C:/SFDC_Enhancement_Automation/Salesforce_Store/Results/HTMLtLog.txt"));
        	
                File f = new File("C:/Users/SYSTCSSR/Desktop/SP2_TC01/Template_6.htm");
                BufferedWriter bw = new BufferedWriter(new FileWriter(f));
               // bw.write("<html>");
                //bw.write("<body>");
                //bw.write("<h1>ShowGeneratedHtml source</h1>");
                //bw.write("<textarea cols=75 rows=30>");
                String line;
                while ((line = br.readLine()) != null) {
                    bw.write(line);
                    bw.newLine();
                }
                String date=getCurrentDate.getISTDate1();
                String projectname=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectName");
                String details="<body><table cellSpacing='0' cellPadding='0' width='96%' border='0'  align='center' style='height: 40px'><tr><td   align=center><span class='heading'>Test Execution Results </span><br /></td></tr></table><table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%; margin-left:20px;'><tr><td class='subheading1' colspan=6 align=center>"+date+"</td></tr><tr><td class = subheading2 colspan =7 align ='center'>"+projectname+"</td></tr><tr><td class='subheading'>Serial Number</td><td class='subheading'>Step Description</td><td class='subheading'>Output Value</td><td class='subheading'>Result</td><td class='subheading'>Time of Execution</td><td class='subheading'>Screenshot</td></tr>";
                
                bw.write(details);
                bw.newLine();
                
                while ((line = br1.readLine()) != null) {
                    bw.write(line);
                    bw.newLine();
                }

                //bw.write("</text" + "area>");
               // bw.write("</body>");
               // bw.write("</html>");

                br.close();
                bw.close();
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
    */
    
    public static void collateHTMLLog(String TestCaseNo,String ResultPath)
    {
    	try
    	{
    		String logo_path="S:/Morrisons Automation(UFT)/Framework/logo_Html.png";
    		String FilePath=ResultPath+"HTMLtLog.txt";
    		
        	BufferedReader br1 = new BufferedReader(
                    new FileReader(FilePath));
        	
                File f = new File(ResultPath+"HTMLtLog.htm");
                BufferedWriter bw = new BufferedWriter(new FileWriter(f));
                String line;
                String date=getCurrentDate.getISTDate1();
                String projectname=ProjectConfigurations.LoadProperties("ProjectName");
                String details0="<html><style>.subheading {BORDER-RIGHT: #8eb3d8 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #8eb3d8 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #8eb3d8 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #8eb3d8 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 30px;BACKGROUND-COLOR: #70DDD3}.subheading1{BORDER-RIGHT: #8eb3d8 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #8eb3d8 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #8eb3d8 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #8eb3d8 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 10px;}.subheading2{BORDER-RIGHT: #8eb3d8 1px solid;PADDING-RIGHT: 2px;BORDER-TOP: #8eb3d8 1px solid;PADDING-LEFT: 2px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #8eb3d8 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #8eb3d8 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 10px;}.subheading3{BORDER-RIGHT: #8eb3d8 1px solid;PADDING-RIGHT: 2px;BORDER-TOP: #8eb3d8 1px solid;PADDING-LEFT: 2px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #8eb3d8 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #8eb3d8 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 10px;BACKGROUND-COLOR: #F4F47F}.tdborder_1{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px}.tdborder_1_PASS{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #ffffff;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;BACKGROUND-COLOR: #358700}.tdborder_1_FAIL{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #ffffff;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;BACKGROUND-COLOR: #f11d16}.tdborder_1_START{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #780000;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;BACKGROUND-COLOR: #F4F47F}.tdborder_1_END{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #15400B;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;BACKGROUND-COLOR: #BABABA}.tdborder_1_Skipped{BORDER-RIGHT: #bdd0e4 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #bdd0e4 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 9pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #bdd0e4 1px solid;COLOR: #0A0A0A;PADDING-TOP: 0px;BORDER-BOTTOM: #bdd0e4 1px solid;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;BACKGROUND-COLOR: #D5E7F2}.heading {FONT-WEIGHT: bold; FONT-SIZE: 17px; COLOR: #005484;FONT-FAMILY: Arial, Verdana, Tahoma, Arial;}.style1 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;COLOR: #000000;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;width: 80px;}.style3 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 9pt;COLOR: #000000;FONT-FAMILY: Arial, helvetica, sans-serif;HEIGHT: 20px;width: 2px;}</style><head><img src='"+logo_path+"' align='right' style='float:right;width:119px;hight:62px'><title></title></head>";
                String details1="<body><table cellSpacing='0' cellPadding='0' width='96%' border='0'  align='center' style='height: 40px'><tr><td   align=center><span class='heading'>Test Execution Results </span><br /></td></tr></table><table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%; margin-left:20px;'><tr><td class='subheading1' colspan=6 align=center>"+date+"</td></tr><tr><td class = subheading2 colspan =7 align ='center'>"+projectname+"</td></tr><tr><td class='subheading'>Serial Number</td><td class='subheading'>Step Description</td><td class='subheading'>Output Value</td><td class='subheading'>Result</td><td class='subheading'>Time of Execution</td><td class='subheading'>Evidence Path Link</td></tr>";
                bw.write(details0);
                bw.write(details1);
                
                while ((line = br1.readLine()) != null) {
                    bw.write(line);
                    bw.newLine();
                }

                bw.write("</tr>");
                bw.write("</table>");
                bw.write("</body>");
                bw.write("</html>");

                bw.close();
                br1.close();
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
}
