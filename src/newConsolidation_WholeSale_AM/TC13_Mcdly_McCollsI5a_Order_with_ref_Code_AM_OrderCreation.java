package newConsolidation_WholeSale_AM;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;




import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;




import I5A_OrderConsolidationUtilities.McColls_I5a_GetCall;
import I5A_OrderConsolidationUtilities.McColls_I5a_PostCall;
import McColls_I5a_All_Functions.McColls_I5a_Utilities;
import Utilities_i5A_All.*;
import I5A_OrderConsolidationUtilities.*;
import McColls_I5a_All_Functions.McColls_I5a_Utilities;

public class TC13_Mcdly_McCollsI5a_Order_with_ref_Code_AM_OrderCreation {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;
	
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	
	String TestDataPath;
	String TemporaryFilePath;
	
	
	
	String TemporaryConsolidationFilePath="";
	
	

	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}



	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		

		utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		
		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_AM");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
		
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");
		
		
	
		
		System.out.println(BrowserPath);
		
		
		ResultPath=getResultPath();

		
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		utilityFileWriteOP.writeToLog("*********************************End**********************************");
		
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = "FAIL";
		String TestKeyword = "N_OrderConsolidation_AM";
		
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;
		
		McColls_I5a_Utilities.OrderIdGenerationI5aConsolidation(TestDataPath, TestKeyword, "Test Data: OrderID Creation", ResultPath)	;
			
		ItemDetailsSheetName=TestKeyword;
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;
		
		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;
		
		String ColumnNames = null;
		String ItemIDs = null;
		
		String Quantities="0";


		

		String consolidatedScreenshotpath="";
		//-------------------------------------------------
		String OrderCount="";   // Default value

		int r = 0;
		try {


			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			System.out.println("Sheet 1 name:"+sheet1);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);
				TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
				TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
//				Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();


				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					
					
					
					Sheet sheet2 = wrk1.getSheet(TestKeyword);	
					int NoOfRows= sheet2.getRows();
					
					//	 System.out.println("Hi");
						
						int NoOfColumns= sheet2.getColumns();
						 
						 
					    System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);
					    
					    System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);
					
					String OrderID=null;
				    int Dt  = (int) new Date().getTime();
					
					String intDt=String.valueOf(Dt);
				    
				    String tFileName="TempResultFile/"+intDt+".csv";
//				    
//				    Sheet sheet2 = wrk1.getSheet(SheetName);
				    
				    System.out.println("The sheet 2 name retrieved is :"+sheet2);

					occurances=occurances+1;
					
					Cell[] FirstRow1 = sheet2.getRow(0);
					Map<String, Integer> map2 = new HashMap<String, Integer>();
					for(int i=0; i < FirstRow1.length; i++)
					{
						map2.put(FirstRow1[i].getContents().trim(), i);
					}
					
					for(r=1; r<NoOfRows; r++) {
//					Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();
					
					
					OrderID=sheet2.getCell(map2.get("OrderID"), r).getContents().trim();
					ProxyHostName=sheet2.getCell(map2.get("ProxyHostName"), r).getContents().trim();
					ProxyPort = sheet2.getCell(map2.get("ProxyPort"), r).getContents().trim();
					SYSUserName = sheet2.getCell(map2.get("SYSUserName"), r).getContents().trim();
					SYSPassWord = sheet2.getCell(map2.get("SYSPassWord"), r).getContents().trim();
					
					TargetHostName = sheet2.getCell(map2.get("TargetHostName"), r).getContents().trim();
					
					TargetPort = sheet2.getCell(map2.get("TargetPort"), r).getContents().trim();
					
					TargetHeader = sheet2.getCell(map2.get("TargetHeader"), r).getContents().trim();
					
					UrlTail = sheet2.getCell(map2.get("UrlTail"), r).getContents().trim();

					ApiKey = sheet2.getCell(map2.get("ApiKey"), r).getContents().trim();
					
					AuthorizationKey = sheet2.getCell(map2.get("AuthorizationKey"), r).getContents().trim();
					AuthorizationValue =sheet2.getCell(map2.get("AuthorizationValue"), r).getContents().trim();
					
					
					
					
					AllMessageTypes = sheet2.getCell(map2.get("MessageType"), r).getContents().trim();
					
					AllShipToLocationIds = sheet2.getCell(map2.get("ShipToLocationId"), r).getContents().trim();
					
					ShipToDeliverAt = sheet2.getCell(map2.get("ShipToDeliverAt"), r).getContents().trim();
								
					AllOrderAttributes = sheet2.getCell(map2.get("orderBuyerPartyId,orderSellerPartyId,orderReferenceCode"), r).getContents().trim();

					AllMessageAttributes = sheet2.getCell(map2.get("messageId,messageSenderPartyId,messageRecipientPartyId,messageCreatedAt"), r).getContents().trim();
					
					AllShipToAddressAttributes = sheet2.getCell(map2.get("shipToPartyId,shipToAddressName,shipToAddressLine1,shipToAddressLine2,shipToAddressCity,shipToAddressState,shipToAddressPostCode,shipToAddressCountryCode,shipToDeliverLatestAt,shipFromLocationId,shipFromAddressName"), r).getContents().trim();
					
					
					AllBillToAddressAttributes = sheet2.getCell(map2.get("billToPartyId,billToAddressName,billToAddressLine1,billToAddressLine2,billToAddressCity,billToAddressState,billToAddressPostCode,billToAddressCountryCode"), r).getContents().trim();
					AllBillToTaxAddressAttributes = sheet2.getCell(map2.get("billToTaxId,billToTaxAddressName,billToTaxAddressLine1,billToTaxAddressLine2,billToTaxAddressCity,billToTaxAddressState,billToTaxAddressPostCode,billToTaxAddressCountryCode"), r).getContents().trim();

					SFTPHostName = sheet2.getCell(map2.get("SFTPHostName"), r).getContents().trim();
					SFTPPort = sheet2.getCell(map2.get("SFTPPort"), r).getContents().trim();
					SFTPUserName = sheet2.getCell(map2.get("SFTPUserName"), r).getContents().trim();
					SFTPPassword = sheet2.getCell(map2.get("SFTPPassword"), r).getContents().trim();
					NasPath = sheet2.getCell(map2.get("NasPath"), r).getContents().trim();
					
					ColumnNames = sheet2.getCell(map2.get("ColumnName"), r).getContents().trim();
					
					String AllItemIDs = sheet2.getCell(map2.get("ItemID"), r).getContents().trim();
					String QuantityType=sheet2.getCell(map2.get("quantityType"), r).getContents().trim();
					String AllShipQty = sheet2.getCell(map2.get("QuantityOrdered"), r).getContents().trim();
					String  ExpOrderStatus=null;
					

					OrderCount=sheet2.getCell(map2.get("OrderCount"), r).getContents().trim();
					
					String ItemValidationStatus = sheet2.getCell(map2.get("Item Type"), r).getContents().trim();
					
					System.out.println("Order "+AllOrderAttributes);
					String [] OrderAttributes=AllOrderAttributes.split(",");
					System.out.println("Order Attributes"+OrderAttributes);
					String OrdRefCode=OrderAttributes[2];
					
					
					String [] Itemss=AllItemIDs.split(",");
					
					
					String qtys[]=QuantityType.split(",");
					
					
					String []AllShipQtys=AllShipQty.split(",");
					
					
		            consolidatedScreenshotpath=getTCFolderPath()+"/";

					System.setProperty(DriverName,DriverPath);

				int itemDetailsPrintOccurence = 0;
				
				
				//-------------------HTML Header--------------------
			 
				Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
									
				//----------------------------------------------------

					

				String []ItemValidationStatusList=ItemValidationStatus.split(",");
				
				
				int trackingNegative=0;
				int trackingPositive=0;
				
				
				for(int t=0;t<ItemValidationStatusList.length;t++){
					

				      if(ItemValidationStatusList[t].contains("N")){
					
				    	  trackingNegative=trackingNegative+1;
				 
				      }
				  
				      if(ItemValidationStatusList[t].contains("P")){
				    		
				    	  trackingPositive=trackingPositive+1;

				      }
				}
					
					if(trackingPositive==ItemValidationStatusList.length){
					
					
						
						
						ExpOrderStatus="confirmed";

					}
						
					
					else{
					
						ExpOrderStatus="shipped-partial";
						
						
					}

				
				

					
				 for(int i=0; i<Itemss.length; i++) {
					 

						
					 String RestGetItemDetails = McColls_I5a_GetCall.GetCallItemDetailsFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, Itemss[i], "skuPin",qtys[i], String.valueOf(i+1), AllShipQtys[i], TestCaseNo, ResultPath);
					 

					 excelCellValueWrite.writeValueToCell(RestGetItemDetails, r, 32+i, TestDataPath, ItemDetailsSheetName);
					 
					 
			  }
				

				
				 Boolean RestPost = McColls_I5a_PostCall.PostCallOrderCreation_Negative_SC(TestDataPath, ItemDetailsSheetName, ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, OrderCount,AllMessageTypes, AllShipToLocationIds, ShipToDeliverAt, AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes,AllItemIDs, r,TemporaryConsolidationFilePath,Keyword, 
						 
						 tFileName,TestCaseNo,ResultPath,ItemValidationStatus,xwpfRun);
				 
				 
				 
				 if(RestPost==true) {
					 
					 logger.log(LogStatus.PASS, "Order Created Successfully.");
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
					}
							
				 else{
					 
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
					 logger.log(LogStatus.FAIL, "Error Occured during Order Creation");
					 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
				 
				 }
				 
				 

				Boolean OrderStatus= McColls_I5a_GetCall.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID,ExpOrderStatus, TestCaseNo,ResultPath,xwpfRun);
				if(OrderStatus==true) {
					logger.log(LogStatus.PASS, "Order Status Validated Successfully.");
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Status Validation", "Order Status Validated Successfully.", "PASS", ResultPath);
					}
							
				 else{
					 logger.log(LogStatus.FAIL, "Error Occured during Order Status Validation");
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Status Validation", "Error Occured during Order Status Validation.", "FAIL", ResultPath);
					 
					 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				 }

				 Boolean RestGet = McColls_I5a_GetCall.GetCallItemStatusValidationWithInvalidItems(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, "confirmed",ItemValidationStatus, TestCaseNo,ResultPath,xwpfRun);
				
				 
				 if(RestGet==true) {
					 
					 logger.log(LogStatus.PASS, "Item Status Validated Successfully.");
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
					}
							
				 else{
					 
//					 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
					 logger.log(LogStatus.FAIL, "Error Occured during Item Status Validation");
					 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				 
				 }
				 

				
				System.out.println("Order ID "+OrderID+"Created");
				
				
				Final_Result = "PASS";
				


				// excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

				
				 
				 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);

			               }
			          
						
						}

			     

			}
					
				}
					
					
			  catch(Exception e) {
						e.printStackTrace();


						
						 Final_Result = "FAIL";
						 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
						 
						 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
				   }
					

				}

			}