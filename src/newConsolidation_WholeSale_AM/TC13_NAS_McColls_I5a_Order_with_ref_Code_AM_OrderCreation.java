package newConsolidation_WholeSale_AM;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


import I5A_OrderConsolidationUtilities.McColls_I5a_Utilities;
import McColls_I5A_OrderCreation_Utility.Consolidation;
import McColls_I5a_All_CommonFunctions.McColls_I5a_Utilities_NAS;
import Utilities.*;
import Utilities_i5A_All.ProjectConfigurations;
import Utilities_i5A_All.Reporting_Utilities;
import Utilities_i5A_All.RowGenerator;
import Utilities_i5A_All.excelCellValueWrite;
import Utilities_i5A_All.utilityFileWriteOP;



public class TC13_NAS_McColls_I5a_Order_with_ref_Code_AM_OrderCreation {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;


	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;

	String TestDataPath;
	String TemporaryFilePath;
	String TemporaryConsolidationFilePath="";
	Integer stepnum=0;
	String downloadFilepath="";
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String ChromeBrowserExePath=null;



	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}



	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_AM");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");

		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");


		System.out.println(BrowserPath);

		if(ServerName.equalsIgnoreCase("Server1")){

			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}


		if(ServerName.equalsIgnoreCase("Server2")){

			ResultPath=utilityFileWriteOP.ReadResultPathServer2();

		}

		ResultPath=getResultPath();


	}

	@After
	public void tearDown() throws Exception {

		}

	@Test
	public void test() throws InterruptedException,IOException {

		Sheet sheet2=null;
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = "FAIL";
		String TestKeyword = "N_OrderConsolidation_AM";

		Boolean resConsolidation_NegSC=false;


		ItemDetailsSheetName=TestKeyword;
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;

		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;

		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String ShipToLocationIds = null;
		String ShipToDeliverAt = null;

		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;

		String SFTPHostName = null;
		String   SFTPPort =null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;

		String ColumnNames = null;
		String ItemIDs = null;

		String Quantities="0";

		String OrderCount="";   // Default value

		String CustomerID="mccolls";
		String ItemTypeList="";

		String TempOrderID_NAS=null;





		String consolidatedScreenshotpath="";
		//-------------------------------------------------


		int r = 0;
		try {


			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);


				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					
				}

					consolidatedScreenshotpath=getTCFolderPath()+"/";

					System.setProperty(DriverName,DriverPath);


					if(Keyword.equalsIgnoreCase(TestKeyword)) {

						occurances=occurances+1;


						sheet2 = wrk1.getSheet(TestKeyword);

						Sheet sheet3 = wrk1.getSheet("Env");	 

						int NoOfRows= sheet2.getRows();
						int NoOfColumns= sheet2.getColumns();

						System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);

						System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);


						String OrderID=null;


						//  NoOfRows=2;


						String  TempFilePath= sheet3.getCell(0, 0).getContents().trim();


						for(int a=1;a<NoOfRows;a++){


							OrderID=sheet2.getCell(0, a).getContents().trim();
							ProxyHostName = sheet2.getCell(1, a).getContents().trim();
							ProxyPort = sheet2.getCell(2, a).getContents().trim();
							SYSUserName = sheet2.getCell(3, a).getContents().trim();
							SYSPassWord = sheet2.getCell(4, a).getContents().trim();

							TargetHostName = sheet2.getCell(5, a).getContents().trim();

							TargetPort = sheet2.getCell(6, a).getContents().trim();

							TargetHeader = sheet2.getCell(7, a).getContents().trim();

							UrlTail = sheet2.getCell(8, a).getContents().trim();

							ApiKey = sheet2.getCell(9, a).getContents().trim();

							AuthorizationKey = sheet2.getCell(10, a).getContents().trim();
							AuthorizationValue =sheet2.getCell(11, a).getContents().trim();

							AllMessageTypes = sheet2.getCell(12, a).getContents().trim();

							ShipToLocationIds = sheet2.getCell(13, a).getContents().trim();

							ShipToDeliverAt = sheet2.getCell(14, a).getContents().trim();

							AllOrderAttributes = sheet2.getCell(15, a).getContents().trim();

							AllMessageAttributes = sheet2.getCell(16, a).getContents().trim();

							AllShipToAddressAttributes = sheet2.getCell(17, a).getContents().trim();
							AllBillToAddressAttributes = sheet2.getCell(18, a).getContents().trim();
							AllBillToTaxAddressAttributes = sheet2.getCell(19, a).getContents().trim();

							SFTPHostName = sheet2.getCell(20, a).getContents().trim();
							SFTPPort = sheet2.getCell(21, a).getContents().trim();
							SFTPUserName = sheet2.getCell(22, a).getContents().trim();
							SFTPPassword = sheet2.getCell(23, a).getContents().trim();
							NasPath = sheet2.getCell(24, a).getContents().trim();

							ColumnNames = sheet2.getCell(25, a).getContents().trim();

							ItemTypeList= sheet2.getCell(31, a).getContents().trim();



							String AllItemIDs = sheet2.getCell(26, a).getContents().trim();


							ShipToLocationIds=sheet2.getCell(30, a).getContents().trim();


							String [] OrderAttributes=AllOrderAttributes.split(",");

							String OrdRefCode=OrderAttributes[2];

							/*
				File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
				screenshotpath.mkdirs();*/


							HashMap<String, String> hm = null;


							hm = new HashMap<String, String>();

							hm=Consolidation.LoadItemsNAS(TempFilePath,hm);


							System.out.println("Hash Map is "+hm);

							int ItemCount= hm.size();


							System.out.println(ItemCount);

							System.out.println(hm);

							String LocalPath="TempDataFiles/";


							List<String> l = new ArrayList<String>(hm.keySet());

							String[] itemsList = new String[l.size()];


							for(int p1=0;p1<l.size();p1++){


								itemsList[p1]=l.get(p1);

							}

							//-------------------HTML Header--------------------

							//				Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);

							//----------------------------------------------------

							ArrayList<HashMap<String, String>> TemporaryOrderIDExtract = McColls_I5a_Utilities_NAS.NASContent_RetriveNegativeSc(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, NasPath,ShipToDeliverAt,LocalPath,OrdRefCode, ShipToLocationIds, l, ItemTypeList,AllItemIDs,TestCaseNo);

							System.out.println( TemporaryOrderIDExtract.size());

							String [] ItemsAll_Type=ItemTypeList.split(",");

							String [] ItemsAll=AllItemIDs.split(",");

							String[] ItemsListValid = new String[50];


							for(int k=0;k<ItemsAll_Type.length;k++){


								if(ItemsAll_Type[k].contentEquals("P")){


									ItemsListValid[k]="P";


								}

							}


							//int NoOfRecordsInNAS=0;

							int NoOfValidItems=ItemsAll_Type.length;


							if(NoOfValidItems==itemsList.length){


								// utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items not Present in the Consolidated File as expected ","PASS");

								resConsolidation_NegSC=true;

							}

							else {

								//utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items are  Present in the Consolidated File  ","FAIL");

								resConsolidation_NegSC=false;


							}


							int cnt=0;

							int PassCount=0;

							for(int p1=0;p1<itemsList.length;p1++){



								for(int u=0;u<TemporaryOrderIDExtract.size();u++){



									String its=TemporaryOrderIDExtract.get(u).get("itemId").toString();



									if(itemsList[p1].contentEquals(its)){

										// shipToLocationId ,CNV ,shipToDeliverAt,itemId,itemAlternateId,itemDescription,quantityType,quantityOrdered

										String qty=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();


										String shipToLocationId_NAS=TemporaryOrderIDExtract.get(u).get("shipToLocationId").toString();

										String messageId_NAS=TemporaryOrderIDExtract.get(u).get("messageId").toString();

										//String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();

										String orderReferenceCode_NAS=TemporaryOrderIDExtract.get(u).get("orderReferenceCode").toString();

										String quantities_NAS=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();

										TempOrderID_NAS=TemporaryOrderIDExtract.get(u).get("orderId").toString();

										String customerId_NAS=TemporaryOrderIDExtract.get(u).get("customerId").toString();

										String quantityType_NAS=TemporaryOrderIDExtract.get(u).get("quantityType").toString();

										String ShipToDeliverAt_NAS=TemporaryOrderIDExtract.get(u).get("shipToDeliverAt").toString();

										String createdAt_NAS=TemporaryOrderIDExtract.get(u).get("createdAt").toString();


										String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();


										String itemAlternateId_NAS=TemporaryOrderIDExtract.get(u).get("itemAlternateId").toString();

										String [] BarcodeEAN=	itemAlternateId_NAS.split("\\|"); 


										if(u==0){



											for(  int k=1;k<sheet2.getRows();k++){

												String OrderIDP=sheet2.getCell(0, k).getContents().trim(); 

												boolean resTempOrderIDValidation=McColls_I5a_Utilities.ValidateTemporaryOrderIDMAPWithOrderIDInRDS(TempOrderID_NAS,OrderIDP, TestCaseNo,ResultPath,xwpfRun);


												if(resTempOrderIDValidation==true){

													//PassCount=PassCount+1;
													
													logger.log(LogStatus.PASS, "Temporary Order ID   "+ TempOrderID_NAS +"is mapped with  "+OrderIDP+"in RDS");

//													utilityFileWriteOP.writeToLog(TestCaseNo, "Temporary Order ID   "+ TempOrderID_NAS +"is mapped with  "+OrderIDP+"in RDS","",ResultPath,xwpfRun,"");

												}  

												else {

													logger.log(LogStatus.FAIL, "Temporary Order ID   "+ TempOrderID_NAS +"is not mapped with  "+OrderIDP+"in RDS");

//													utilityFileWriteOP.writeToLog(TestCaseNo, "Temporary Order ID   "+ TempOrderID_NAS +"is not mapped with  "+OrderIDP+"in RDS","",ResultPath,xwpfRun,"");

												}

											}  

										} 


										//2018-02-26


										String DATE_FORMAT = "yyyy-dd-mm";
										SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
										//  System.out.println("Formated Date " + sdf.format(date));

										System.out.println(ShipToDeliverAt_NAS);



										if(BarcodeEAN[1].contentEquals("clientId:wholesale")){

											PassCount=PassCount+1;

											System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"matches with "+"clientId:wholesale");
											
											logger.log(LogStatus.PASS, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Success");


//											utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale", "Success");

//											utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale","",ResultPath,xwpfRun,"");



										}


										else {

											System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"not matches with "+"clientId:wholesale");
											
											logger.log(LogStatus.FAIL, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Fail");
											/*utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale", "Fail");

											utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale","",ResultPath,xwpfRun,"");
*/

										} 




										if(BarcodeEAN[0].contentEquals("barcodeEan:wholesale")){

											PassCount=PassCount+1;

											System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"matches with "+"barcodeEan:wholesale");
											
											logger.log(LogStatus.PASS,  "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale"+": Success");
/*
											utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale", "Fail");
											utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");
*/
										}

										else {

											System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"not matches with "+"barcodeEan:wholesale");
											logger.log(LogStatus.FAIL,  "barcodeEan in NAS   "+ BarcodeEAN[0] +" not matches with barcodeEan:wholesale"+": Fail");
											
//											utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale", "Success");
//											utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");
										}

										if(itemDescription_NAS.contentEquals("Optional")){

											PassCount=PassCount+1;

											System.out.println("itemDescription in NAS  "+ itemDescription_NAS +"matches with "+"Optional");
											logger.log(LogStatus.PASS,"itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional"+": Success");
											/*utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional", "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional","",ResultPath,xwpfRun,"");
*/
										}


										else {

											System.out.println("itemDescription in NAS  "+ itemDescription_NAS +" not matches with "+"Optional");
											logger.log(LogStatus.FAIL,  "barcodeEan in NAS   "+ BarcodeEAN[0] +"Not matches with barcodeEan:wholesale"+": FAIL");
//											utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional", "FAIL");
//											utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional","",ResultPath,xwpfRun,"");
										}  	   

										if(ShipToDeliverAt.contentEquals(ShipToDeliverAt_NAS)){

											PassCount=PassCount+1;

											System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt);
											logger.log(LogStatus.PASS,"ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt+": Success");
											/*
											utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt, "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");
*/
										}
										else {

											System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt);
											logger.log(LogStatus.FAIL, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt+": FAIL");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt, "FAIL");
											utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");*/
										} 	

										if(OrdRefCode.contentEquals(orderReferenceCode_NAS)){

											PassCount=PassCount+1;

											System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode);
											logger.log(LogStatus.PASS,"orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode+": Success");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode, "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");*/
										}
										else {

//											utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode, "Fail");
											System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode);
											logger.log(LogStatus.FAIL, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode+": FAIL");

//											utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");
										}

										if(CustomerID.contentEquals(customerId_NAS)){

											PassCount=PassCount+1;

											System.out.println("CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID);
											logger.log(LogStatus.PASS,"CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID+": Success");
											
											/*utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID , "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"matches with "+CustomerID,"",ResultPath,xwpfRun,"");*/
										}

										else {

											System.out.println("CustomerID in NAS  "+ CustomerID +"not matches with "+CustomerID);
											logger.log(LogStatus.FAIL, "CustomerID in NAS  "+ customerId_NAS +"not matching  with "+CustomerID+": FAIL");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"not matching  with "+CustomerID , "Fail");
											utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"not matches with "+CustomerID,"",ResultPath,xwpfRun,"");*/
										}   	

										if(ShipToLocationIds.contentEquals(shipToLocationId_NAS)){

											PassCount=PassCount+1;
											System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds);
											logger.log(LogStatus.PASS,"shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds+": Success");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds , "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");*/
										}

										else {
											System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matches with "+ShipToLocationIds);
											logger.log(LogStatus.FAIL, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds+": FAIL");
											/*
											utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds , "Fail");
											utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"not matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");*/
										}

										if(quantityType_NAS.contentEquals("CS")){

											PassCount=PassCount+1;
											System.out.println("quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS");

											logger.log(LogStatus.PASS, "quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS"+": Success");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS" , "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"matches with "+"CS","",ResultPath,xwpfRun,"");*/

										}
										else {
											System.out.println("quantityType in NAS  "+ quantityType_NAS +"  not matches with "+"CS");
											logger.log(LogStatus.FAIL, "quantityType in NAS  "+ quantityType_NAS +"  not matching  with "+"CS"+": FAIL");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  not matching  with "+"CS" , "Fail");
											utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"not matches with "+"CS","",ResultPath,xwpfRun,"");*/
										}

										String  exp_qty=hm.get(its).toString();

										String CS_QtyType= Consolidation.getCaseSize_And_ItemType(TempFilePath, its);

										String [] CS_QtyTypeArray=CS_QtyType.split("#");

										if((CS_QtyTypeArray[0]).contains("EA")){

											int Qt=0; 

											int Qty=Integer.parseInt(exp_qty);

											int CaseSize=Integer.parseInt(CS_QtyTypeArray[1]);

											Qt= ((Qty +CaseSize - 1) / CaseSize);

											exp_qty=String.valueOf(Qt);

										}

										if(exp_qty.contentEquals(qty)){

											PassCount=PassCount+1;

											System.out.println("Consolidated qty  for Item "+its+"is   "   +qty+" and it matches with expected quantity "+exp_qty);
											logger.log(LogStatus.PASS,"Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty+": Success");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty , "Success");
											utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");*/
										}

										else {

											System.out.println("Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching  with expected quantity "+exp_qty);
											logger.log(LogStatus.FAIL, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty+": FAIL");

											/*utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty , "Fail");
											utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and is not matching  with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");*/
										}

										break;
									}
								}
							}

							if((PassCount==(9*itemsList.length))){


								//   if((PassCount==(6*itemsList.length))&&(resConsolidation_NegSC)){   
								Final_Result = "PASS";

								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "NAS File Validated Successfully.", "PASS", ResultPath);
								excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

								 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);

									logger.log(LogStatus.PASS,"Consolidated file validation "+": Success");

//								utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "Success");

								System.out.println("Consolidated file validation PASS");

//								utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation PASS","",ResultPath,xwpfRun,"");

							}


							else{

							Final_Result = "FAIL";

								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "Error Occured during NAS File Validation.", "FAIL", ResultPath);

								excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

								 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
									logger.log(LogStatus.FAIL,"Consolidated file validation "+": FAIL");
//								utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "FAIL");

								System.out.println("Consolidated file validation FAIL");
								
//								utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation FAIL","",ResultPath,xwpfRun,"");
							} 

						}

					}

				}
		}
				catch(Exception e) {
					e.printStackTrace();


					Final_Result = "FAIL";
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

					 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
				}

			}

		}			
