package newConsolidation_WholeSale_AM;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Utilities.GetTCDetailsFromDataSheet;
import Utilities.ProjectConfigurations;
import Utilities.Reporting_Utilities;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Copy {

	@Test
	public void test_ExecuteTest_McColls_I5A() throws IOException {

		ExtentReports extent=null;
		ExtentTest logger=null;
		String ResultPath=null;
		XWPFRun xwpfRun=null;
		XWPFDocument doc=null;
		
		String Final_Result="FAIL";
		
		
		String  TestKeyword = "N_OrderConsolidation_AM";

		String TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_AM");
		String SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
		
		String TestCaseNo=GetTCDetailsFromDataSheet.TCDetails(TestKeyword,TestDataPath,SheetName).get("TestCaseNo");
		String TestCaseName=GetTCDetailsFromDataSheet.TCDetails(TestKeyword,TestDataPath,SheetName).get("TestCaseName");
		

		if(ResultPath==null){

			String ResPath=System.getProperty("user.dir") +"/Results"+"/"+"WholeSale_AM_"+getCurrentDate.getISTDateddMM();

			ResultPath=Reporting_Utilities.createResultPath(ResPath);	
			//ResultPath=Reporting_Utilities.createResultPath("NAS "+getCurrentDate.getISTDateddMM()+"/run_RMSE_RPAS_ITEM_MASTER");

			ResultPath.replace("\\", "/");
			System.setProperty("resultpath",ResultPath);
		}

		System.out.println("Result path is "+ResultPath);


		String TC_ScFolder=ResultPath+TestCaseNo;

		System.out.println("Sc result path is "+TC_ScFolder);


		File dir = new File(TC_ScFolder);

		if(!dir.exists()){

			dir.mkdir();

		}

		try{

			
			doc = new XWPFDocument();
	        XWPFParagraph p = doc.createParagraph();
	        xwpfRun = p.createRun();  
			
			
			extent = new ExtentReports (ResultPath+"AM_SeleniumReport.html", false);
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
			//		 //*******************************************************************************************************
			//		//****************************Instantiate Extent Reporting************************
			logger=extent.startTest(TestCaseNo+":"+TestCaseName);


			newConsolidation_WholeSale_AM.McColls_I5a_CleanupJob_Run.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep1 = JUnitCore.runClasses(newConsolidation_WholeSale_AM.McColls_I5a_CleanupJob_Run.class);

			boolean step1= ResStep1.wasSuccessful();
			
			
			newConsolidation_WholeSale_AM.TC13_Mcdly_McCollsI5a_Order_with_ref_Code_AM_OrderCreation.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep2 = JUnitCore.runClasses(newConsolidation_WholeSale_AM.TC13_Mcdly_McCollsI5a_Order_with_ref_Code_AM_OrderCreation.class);

			boolean step2= ResStep2.wasSuccessful();
			
			
			newConsolidation_WholeSale_AM.Wholesale_Consolidation_ECS_Task_Run.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep3 = JUnitCore.runClasses(newConsolidation_WholeSale_AM.Wholesale_Consolidation_ECS_Task_Run.class);

			boolean step3= ResStep3.wasSuccessful();
			
			
			newConsolidation_WholeSale_AM.TC13_NAS_McColls_I5a_Order_with_ref_Code_AM_OrderCreation1.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep4 = JUnitCore.runClasses(newConsolidation_WholeSale_AM.TC13_NAS_McColls_I5a_Order_with_ref_Code_AM_OrderCreation1.class);

			boolean step4= ResStep4.wasSuccessful();
			
			

			if(step1 && step2 && step3 && step4){

				
				 Final_Result="PASS";
					logger.log(LogStatus.PASS, "TestCaseNo::"+TestCaseName+":: PASS"); 
					
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);
					
					
				}

				else {

					logger.log(LogStatus.FAIL, "TestCaseNo::"+TestCaseName+":: FAIL"); 
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);

				}

		}


		catch(Exception e){

			System.out.println("The exception is "+e);

		}


		finally{

			extent.endTest(logger);
			extent.flush();
			
			FileOutputStream out1;
		
				out1 = new FileOutputStream(TC_ScFolder+"/"+"AM_"+Final_Result+".docx");
				doc.write(out1);
				out1.close();
				doc.close();

		}



	}

}
