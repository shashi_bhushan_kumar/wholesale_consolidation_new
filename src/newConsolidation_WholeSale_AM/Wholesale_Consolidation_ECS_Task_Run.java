package newConsolidation_WholeSale_AM;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;









import SandPiper_I5a_All_CommonFunctions.SandPiper_I5a_Utilities;
import Utilities.*;
import Utilities_i5A_All.MyException;
import Utilities_i5A_All.ProjectConfigurations;
import Utilities_i5A_All.Reporting_Utilities;
import Utilities_i5A_All.RowGenerator;
import Utilities_i5A_All.getCurrentDate;
import Utilities_i5A_All.utilityFileWriteOP;



public class Wholesale_Consolidation_ECS_Task_Run {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;
	
	
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	
	String TestDataPath;
	String TemporaryFilePath;
	String Screenshotpath;
	ChromeOptions options=null;
	String OrderDetailsSheetName;
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String ChromeBrowserExePath=null;
	
	

	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}



	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		
		ChromeBrowserExePath=ProjectConfigurations.LoadProperties("ChromeBrowserExePath");

		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_AM");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
//		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("SandPiper_I5a_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");
		
		
		
		
		System.out.println(BrowserPath);
		
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}
		
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		
		System.setProperty(DriverName, DriverPath);
	
		ResultPath=getResultPath();

		
	}

	@After
	public void tearDown() throws Exception {
		
//      driver.quit();
      
      //System.exit(1);
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestCaseNo = "ECS Job Run";
		String TestCaseName = "ECS Job Run";

		String Final_Result = null;


		String AwsUsername = null;
		String Awspassword = null;
		String Searchkey = null;
		String jobname = null;

		

		String consolidatedScreenshotpath="";
		//-------------------------------------------------


try{
			

			AwsUsername = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Username");
			Awspassword= ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Password");
			Searchkey = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_SearchKey");
			jobname = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Jobname");
			ChromeBrowserExePath=ProjectConfigurations.LoadProperties("ChromeBrowserExePath");
		

				 boolean login = true;
				 boolean taskrun = true;
				 boolean logut = true;
					
					
					
		            consolidatedScreenshotpath=getTCFolderPath()+"/";

					System.setProperty(DriverName,DriverPath);
					System.out.println("Driver Type:"+DriverType);
					System.out.println("BrowserPath:"+BrowserPath);


					
					if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
						ChromeOptions options = new ChromeOptions();
					//	options.setBinary(ChromeBrowserExePath);

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
						
						driver.manage().deleteAllCookies();
						driver.get("https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console");
						
						driver.manage().window().maximize();
           
						AwsUsername="usr-glob-n-mo-testautomation-user-002";
								
						Awspassword="]S1oeO-|Xju7";
						
					}

					else {
						
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
						ChromeOptions options = new ChromeOptions();
					    
						options.setBinary(ChromeBrowserExePath);  // setup chrome binary path

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
											
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
					
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	

						driver.manage().deleteAllCookies();
						driver.get("https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console");
						
//						driver.manage().window().maximize();
           
						AwsUsername="usr-glob-n-mo-testautomation-user-002";
								
						Awspassword="]S1oeO-|Xju7";
						
						
					}
					
					
					
					boolean AwsLogin = SandPiper_I5a_Utilities.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, Screenshotpath, ResultPath,xwpfRun);
					
					 if(AwsLogin==true) {
						 logger.log(LogStatus.PASS, "AWS Logged In Successfully");
//						 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "AWS LOGIN", "AWS Logged In Successfully.", "PASS", ResultPath);
						}
								
					else{
						logger.log(LogStatus.FAIL, "Error Occured during AWS Login");
//						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "AWS LOGIN", "Error Occured during AWS Login.", "FAIL", ResultPath);
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					
					
					 login = login && AwsLogin;
						
						
						String[] taskid=new String[1];
						String taskID="";
						boolean LambdaJobRun = SandPiper_I5a_Utilities.Ecs_task_runner_job_trigger_task_ID_Retrieve(driver, wait, TestCaseNo, Searchkey, jobname, ResultPath, Screenshotpath,xwpfRun,taskid);
						
						 if(LambdaJobRun==true) {
							 logger.log(LogStatus.PASS, "ECS Task Runner JOB triggered Successfully");
//							 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "ECS Task Runner JOB triggered Successfully.", "PASS", ResultPath);
							 System.out.println("Outsidecaller script taskid "+taskid[0]);
							 taskID=taskid[0];
						 }
									
						else{
							logger.log(LogStatus.FAIL, "Error Occured during ECS Task Runner JOB");
//							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "Error Occured during ECS Task Runner JOB.", "FAIL", ResultPath);
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}
						
						
						taskrun = taskrun && LambdaJobRun;
						
						
						driver.quit();
				


						 if( (login) && (taskrun) ) {
							 
							 Final_Result = "PASS";

							 
							 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);
						 }
						 else {
							 Final_Result = "FAIL";
							 
							 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
							 
						 }
				}
				
				
				catch(Exception e) {
					e.printStackTrace();

					 Final_Result = "FAIL";
					 
					 Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
				}
				finally
				 {   
					
				 }
					
				}
		}
