package McColls_I5A_OrderCreation_Utility;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;






import McColls_I5a_All_Functions.McColls_I5a_Utilities;
import Utilities_i5A_All.ProjectConfigurations;
import Utilities_i5A_All.getCurrentDate;
import Utilities_i5A_All.utilityFileWriteOP;



public class OrderIDCreation_run {
	
	//String URL;
			String DriverPath;
			String DriverName;
			String DriverType;
			String BrowserPath;
			String ServerName;
			String SheetName;
			String ItemDetailsSheetName;
			
			String TestDataPath;
			String TemporaryFilePath;
			
			String ResultPath="";
			String Screenshotpath;
			
			String TemporaryConsolidationFilePath;

			
			public WebDriver driver = null;
			public WebDriverWait wait = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
				DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
				DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
				DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
				BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
				ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
				TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath");
				SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
				ItemDetailsSheetName=ProjectConfigurations.LoadProperties("McColls_I5a_ItemDetailsSheetName");
				
				
				System.out.println(BrowserPath);
				
				if(ServerName.equalsIgnoreCase("Server1")){
					
					ResultPath=utilityFileWriteOP.ReadResultPathServer1();
				}
				
				
				if(ServerName.equalsIgnoreCase("Server2")){
					
					ResultPath=utilityFileWriteOP.ReadResultPathServer2();
					
				}
		
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		
		
		
		System.setProperty(DriverName, DriverPath);
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************END**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		

	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = null;
		String TestCaseName = null;
		//String Final_Result = null;

		//int rows = 0;
	
		boolean OrderIDGenerationValidation = true;


		try{
			
			//Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			
			//Sheet sheet1 = wrk1.getSheet(SheetName);
			
			//rows = sheet1.getRows();
			
			//for(int r=1; r<rows; r++) {
				
				//String orderSheetname = sheet1.getCell(2, r).getContents().trim();
				
				//Sheet sheetorder = wrk1.getSheet(orderSheetname);

				 Boolean OrderIDGeneration = McColls_I5a_Utilities.OrderIdGenerationI5a(TestDataPath, SheetName, TestCaseNo, ResultPath);

				 //System.out.println(orderSheetname);
				 System.out.println(OrderIDGeneration);
				 if( (OrderIDGeneration)) {
					 
					 OrderIDGenerationValidation = OrderIDGenerationValidation && true;
					 					 
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs for "+SheetName,ResultPath);

					 Assert.assertTrue(TestCaseName, true);
				 }
				 else {
					 OrderIDGenerationValidation = OrderIDGenerationValidation && false;
					 
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs for "+SheetName,ResultPath);

					 Assert.assertTrue(TestCaseName, false);
					 
				 }
				
			//}

				//Add this line as many times needed. change the testdataPath and sheetname	
			/* Boolean OrderIDGeneration = McColls_I5a_Utilities.OrderIdGeneration(TestDataPath, SheetName, TestCaseNo, ResultPath);
	 
			 
				 if( (OrderIDGeneration)) {
					 
					 Final_Result = "PASS";
					 					 
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs ",ResultPath);

					 Assert.assertTrue(TestCaseName, true);
				 }
				 else {
					 Final_Result = "FAIL";
					 
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs ",ResultPath);

					 Assert.assertTrue(TestCaseName, false);
					 
				 }*/
			
		//	wrk1.close();
	
		}
		catch(Exception e) {
			e.printStackTrace();

			// Final_Result = "FAIL";
				utilityFileWriteOP.writeToLog("Order ID Generation Script", "An Error occured while creating Order IDs ",ResultPath);

		 
			 Assert.assertTrue(TestCaseName, false);
		}
		
	}

}
